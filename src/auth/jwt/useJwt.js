// ** Core JWT Import
import useJwt from '@src/@core/auth/jwt/useJwt'

const BASE_URL = 'https://app-trade1.investlink.io/'

const { jwt } = useJwt({
    loginEndpoint: `${BASE_URL}auth/login/`,
    registerEndpoint: `${BASE_URL}auth/register/`,
    refreshEndpoint: `${BASE_URL}auth/token/refresh/`,
    logoutEndpoint: `${BASE_URL}auth/logout/`
})

export default jwt
