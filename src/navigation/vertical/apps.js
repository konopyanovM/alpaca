// ** Icons Import
import { Clipboard, List, Home, Archive, DollarSign, Star } from 'react-feather'

export default [
  {
    header: 'Menu'
  },
  {
    id: 'Overview',
    title: 'Overview',
    action: 'read',
    icon: <Home size={20} />,
    navLink: '/stocks'
  },
  {
    id: 'Watchlist',
    title: 'Watchlist',
    action: 'read',
    icon: <Star size={20} />,
    navLink: '/watchlist'
  },
  {
    id: 'Orders',
    title: 'Orders & Positions',
    action: 'read',
    icon: <List size={20} />,
    navLink: '/orders-positions'
  },
  {
    id: 'Transaction history',
    title: 'Transaction history',
    action: 'read',
    icon: <DollarSign size={20} />,
    navLink: '/transaction-history'
  },
  {
    id: 'Documents',
    title: 'Documents',
    action: 'read',
    icon: <Archive size={20} />,
    navLink: '/documents'
  }
]
