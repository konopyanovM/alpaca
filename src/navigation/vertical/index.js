// ** Navigation imports
import apps from './apps'
import admin from './admin'

// ** Merge & Export
export default [...apps, ...admin]
