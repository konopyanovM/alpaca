// ** Icons Import

import { Bell, Users } from "react-feather"

export default [
  {
    header: 'Admin panel'
  },
  {
    id: 'users',
    title: 'Users',
    icon: <Users size={20} />,
    navLink: '/admin/users'
  },
  {
    id: 'events',
    title: 'Events',
    icon: <Bell size={20} />,
    navLink: '/admin/events'
  }
]
