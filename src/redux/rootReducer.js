// ** Reducers Imports
import layout from './layout'
import auth from './authentication'
import ecommerce from '@src/views/apps/ecommerce/store'
import permissions from '@src/views/apps/roles-permissions/store'

const rootReducer = {
  auth,
  layout,
  ecommerce,
  permissions
}

export default rootReducer
