// ** React Imports
import { Fragment, useState, useEffect } from 'react'

// ** Third Party Components
import { useForm, Controller } from 'react-hook-form'
import 'cleave.js/dist/addons/cleave-phone.us'

// ** Reactstrap Imports
import {
  Row,
  Col,
  Form,
  Card,
  Input,
  Label,
  Button,
  CardBody,
  CardTitle,
  CardHeader,
  FormFeedback
} from 'reactstrap'


// ** Demo Components
import { createACHRel, getAchRelationship } from '../../../utility/Services'
import { X } from 'react-feather'
import toast from 'react-hot-toast'


const ToastContent = ({ t, name, isError, message }) => {
  return (
    <div className="d-flex">
      <div className="d-flex flex-column">
        <div className="d-flex justify-content-between">
          <h6>{name}</h6>
          <X
            size={12}
            className="cursor-pointer"
            onClick={() => toast.dismiss(t.id)}
          />
        </div>
        {
          isError ? <span>
            Ошибка
          </span> : <span>
            {message}
          </span>
        }

      </div>
    </div>
  )
}

const MakeTransaction = ({ data }) => {
  // ** Hooks
  const defaultValues = {
    bank_account_number: '',
    bank_routing_number: ''
  }

  const [account, setAccount] = useState('0')
  const [routing, setRouting] = useState('0')

  const [accountData, setAccountData] = useState({})

  useEffect(() => {
    getAchRelationship().then((res) => {
      const data = res.data[0]
      setAccountData({
        bank_account_number: data.bank_account_number,
        bank_routing_number: data.bank_routing_number,
        bank_account_type: data.bank_account_type,
        status: data.status,
      })
      setAccount(res.data[0].bank_account_number)
      setRouting(res.data[0].bank_routing_number)
    })
  }, [])

  const {
    control,
    setError,
    handleSubmit,
    formState: { errors }
  } = useForm({ defaultValues })
  // ** States
  const onSubmit = (formData) => {
    if (Object.values(formData).every((field) => field.length > 0)) {
      createACHRel(formData).then(() => {
        toast((t) => (
          <ToastContent
            t={t}
            name={data.first_name || data.username || 'John Doe'}
            message='Bank details added'
          />
        ))
      }).catch(() => {
        toast((t) => (
          <ToastContent
            t={t}
            name={data.first_name || data.username || 'John Doe'}
            isError={true}
          />
        ))
      })
      return null
    } else {
      for (const key in formData) {
        if (formData[key].length === 0) {
          setError(key, {
            type: 'manual'
          })
        }
      }
    }
  }

  return (
    <Fragment>
      <Card>
        <CardHeader className="border-bottom">
          <CardTitle tag="h4">Bank Details</CardTitle>
        </CardHeader>
        <CardBody className="py-2 my-25">
          <Row>
            <Col sm="6" className="mb-1">
              <p>Bank account number: {accountData.bank_account_number}</p>
            </Col>
            <Col sm="6" className="mb-1">
              <p>Bank routing number: {accountData.bank_routing_number}</p>
            </Col>
          </Row>
          <Row>
            <Col sm="6" className="mb-1">
              <p>Bank account type: {accountData.bank_account_type}</p>
            </Col>
            <Col sm="6" className="mb-1">
              <p>Status: {accountData.status}</p>
            </Col>
          </Row>
          <Form className="mt-2 pt-50" onSubmit={handleSubmit(onSubmit)}>
            <Row>
              <Col sm="6" className="mb-1">
                <Label className="form-label" for="bank_account_number">
                  Account number
                </Label>
                <Controller
                  name="bank_account_number"
                  control={control}
                  render={({ field }) => (
                    <Input
                      id="bank_account_number"
                      invalid={errors.bank_account_number && true}
                      {...field}
                    />
                  )}
                />
                {errors && errors.bank_account_number && (
                  <FormFeedback>Please enter a account number</FormFeedback>
                )}
              </Col>
              <Col sm="6" className="mb-1">
                <Label className="form-label" for="bank_routing_number">
                  Routing number
                </Label>
                <Controller
                  name="bank_routing_number"
                  control={control}
                  render={({ field }) => (
                    <Input
                      id="bank_routing_number"
                      invalid={errors.bank_routing_number && true}
                      {...field}
                    />
                  )}
                />
                {errors.bank_routing_number && (
                  <FormFeedback>
                    Please enter a valid routing number
                  </FormFeedback>
                )}
              </Col>
              <Col className="mt-2" sm="12">
                <Button type="submit" className="me-1" color="primary">
                  Save changes
                </Button>
                <Button color="secondary" outline>
                  Discard
                </Button>
              </Col>
            </Row>
          </Form>
        </CardBody>
      </Card>
    </Fragment>
  )
}

export default MakeTransaction
