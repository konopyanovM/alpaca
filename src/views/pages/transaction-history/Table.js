// ** React Imports
import { Fragment, useState, useEffect } from 'react'

// ** Table Columns
import { columns } from './columns'
import DataTable from 'react-data-table-component'
import { ChevronDown } from 'react-feather'

// ** Utils
// ** Reactstrap Imports
import {
  Card, Spinner,
} from 'reactstrap'

// ** Styles
import '@styles/react/libs/react-select/_react-select.scss'
import '@styles/react/libs/tables/react-dataTable-component.scss'
import { getTransactionHistory } from '../../../utility/Services'

const UsersList = () => {
  const [list, setList] = useState(null)

  const getTransactions = async () => {
    const { data } = await getTransactionHistory()

    setList(data.results)
  }

  useEffect(() => {
    getTransactions()
  }, [])

  return (
    <Fragment>
      <Card className='overflow-hidden'>
        {list !== null
          ? <div className='react-dataTable'>
            <DataTable
              noHeader
              sortServer
              pagination={false}
              responsive
              paginationServer
              columns={columns}
              sortIcon={<ChevronDown />}
              className='react-dataTable'
              data={list}
            />
          </div>
          : <div className='py-5 d-flex justify-content-center'>
            <Spinner />
          </div>}


      </Card>
    </Fragment>
  )
}

export default UsersList
