// ** User List Component
import Table from './Table'

import Breadcrumbs from '@components/breadcrumbs'

// ** Styles
import '@styles/react/apps/app-users.scss'

const UsersList = () => {
  return <>
    <Breadcrumbs title='Transaction history' data={[{ title: 'Transaction history' }]} />
    <Table />
  </>
}

export default UsersList
