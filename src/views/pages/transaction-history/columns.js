import { Badge } from 'reactstrap'


const renderDirection = (type) => {
  if (type === 'INCOMING') return (
  <Badge color='success' className='d-block'>
    <span>{type}</span>
  </Badge>
  )
  if (type === 'OUTCOMING') return (
    <Badge color='danger' className='d-block'>
      <span>{type}</span>
    </Badge>
    )
    return  <Badge color='primary' className='d-block'>
    <span>{type}</span>
  </Badge>
}

const renderStatus = (type) => {
  if (type === 'COMPLETE') return (
  <Badge color='success' className='d-block'>
    <span>{type}</span>
  </Badge>
  )
  if (type === 'QUEUED') return (
    <Badge color='warning' className='d-block'>
      <span>{type}</span>
    </Badge>
    )
    return (
      <Badge color='primary' className='d-block'>
        <span>{type}</span>
      </Badge>
      ) 
}

const renderDate = (date) => {
  return new Date(date).toLocaleDateString("ru-RU")
}

export const columns = [
  {
    name: 'Type',
    sortable: true,
    sortField: 'first_name',
    selector: row => row.first_name,
    cell: row => {
      return (
        <div className='d-flex justify-content-left align-items-center'>
          {renderDirection(row.direction)}
        </div>
      )
    }
  },
  {
    name: 'Amount',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {row.amount}
    </div>
  },
  {
    name: 'Fee',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {row.fee}
    </div>
  },
  {
    name: 'Status',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
    {renderStatus(row.status)}
  </div>
  },
  {
    name: 'Date',
    cell: row => (
      <div className='column-action'>
        {renderDate(row.created_at)}
      </div>
    )
  }
]