import Breadcrumbs from '@components/breadcrumbs'

// ** Styles
import '@styles/react/apps/app-users.scss'
import { Card, CardBody } from 'reactstrap'

const Disclosures = () => {
  return <>
    <Breadcrumbs title='Disclosures' data={[{ title: 'Disclosures' }]} />
    <Card>
      <CardBody className='d-flex flex-column gap-1'>
        <div className='divider'>
          <div className='divider-text'>Use and Risk</div>
        </div>
        <h5><a href="https://files.alpaca.markets/disclosures/library/UseAndRisk.pdf" className='app-hover' target='_blank'>Use and Risk</a></h5>
        <div className='divider'>
          <div className='divider-text'>Terms and Conditions</div>
        </div>
        <h5><a href="https://files.alpaca.markets/disclosures/library/TermsAndConditions.pdf" className='app-hover' target='_blank'>Terms and Conditions</a></h5>
        <div className='divider'>
          <div className='divider-text'>Privacy Notice</div>
        </div>
        <h5><a href="https://files.alpaca.markets/disclosures/library/PrivacyNotice.pdf" className='app-hover' target='_blank'>Privacy Notice</a></h5>
        <div className='divider'>
          <div className='divider-text'>PFOP</div>
        </div>
        <h5><a href="https://files.alpaca.markets/disclosures/library/PFOF.pdf" className='app-hover' target='_blank'>PFOP</a></h5>
        <div className='divider'>
          <div className='divider-text'>Margin Disclosure Statement</div>
        </div>
        <h5><a href="https://files.alpaca.markets/disclosures/library/MarginDiscStmt.pdf" className='app-hover' target='_blank'>Margin Disclosure Statement</a></h5>
        <div className='divider'>
          <div className='divider-text'>Extended Hours Trading Risk</div>
        </div>
        <h5><a href="https://files.alpaca.markets/disclosures/library/ExtHrsRisk.pdf" className='app-hover' target='_blank'>Extended Hours Trading Risk</a></h5>
        <div className='divider'>
          <div className='divider-text'>Business Continuity Plan Summary</div>
        </div>
        <h5><a href="https://files.alpaca.markets/disclosures/library/BCPSummary.pdf" className='app-hover' target='_blank'>Business Continuity Plan Summary</a></h5>
        <div className='divider'>
          <div className='divider-text'>Form CRS</div>
        </div>
        <h5><a href="https://files.alpaca.markets/disclosures/Form+CRS.pdf" className='app-hover' target='_blank'>Form CRS</a></h5>
      </CardBody>
    </Card>
  </>
}

export default Disclosures
