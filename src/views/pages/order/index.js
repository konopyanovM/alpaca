// ** User List Component
import OrdersTable from './orders'
import PositionsTable from './positions'

// ** Styles
import '@styles/react/apps/app-users.scss'
import { Col, TabContent, TabPane } from 'reactstrap'

// ** Demo Components
import Tabs from './Tabs'
import { useState } from 'react'
import { useLocation } from 'react-router-dom'

const List = () => {

  // ** States
  const query = useLocation().search.split('=')[1]
  const [activeTab, setActiveTab] = useState(query || '1')

  const toggleTab = tab => {
    setActiveTab(tab)
  }

  return (
    <Col xs={12}>
      <Tabs className='mb-2' activeTab={activeTab} toggleTab={toggleTab} />

      <TabContent activeTab={activeTab}>
        <TabPane tabId='1'>
          <OrdersTable></OrdersTable>
        </TabPane>
        <TabPane tabId='2'>
          <PositionsTable />
        </TabPane>
      </TabContent>
    </Col>
  )
}

export default List
