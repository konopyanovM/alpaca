// ** Reactstrap Imports
import { Nav, NavItem, NavLink } from 'reactstrap'

// ** Icons Imports
import { Clipboard, List } from 'react-feather'

const Tabs = ({ activeTab, toggleTab }) => {
  return (
    <Nav pills className='mb-2'>
      <NavItem>
        <NavLink active={activeTab === '1'} onClick={() => toggleTab('1')}>
          <List size={18} className='me-50' />
          <span className='fw-bold'>Orders</span>
        </NavLink>
      </NavItem>
      <NavItem>
        <NavLink active={activeTab === '2'} onClick={() => toggleTab('2')}>
          <Clipboard size={18} className='me-50' />
          <span className='fw-bold'>Positions</span>
        </NavLink>
      </NavItem>
    </Nav>
  )
}

export default Tabs
