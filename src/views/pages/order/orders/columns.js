import { X } from 'react-feather'
import toast from 'react-hot-toast'
import { Link } from 'react-router-dom'
import { Badge, Button } from 'reactstrap'
import { cancelOrder } from '../../../../utility/Services'
import { useForceUpdate } from '../../../../utility/Utils'

const ToastContent = ({ t, name, isError, message }) => {
  return (
    <div className="d-flex">
      <div className="d-flex flex-column">
        <div className="d-flex justify-content-between">
          <h6>{name}</h6>
          <X
            size={12}
            className="cursor-pointer"
            onClick={() => toast.dismiss(t.id)}
          />
        </div>
        {
          isError ? <span>
            Something went wrong.
          </span> : <span>
            {message}
          </span>
        }
      </div>
    </div>
  )
}

const renderSide = (side) => {
  if (side === 'buy') return (
    <Badge color='success' className='d-block'>
      <span>Buy</span>
    </Badge>
  )
  if (side === 'sell') return (
    <Badge color='danger' className='d-block'>
      <span>Sell</span>
    </Badge>
  )
}

const renderStatus = (status) => {
  if (status === 'accepted') return (
    <Badge color='success' className='d-block'>
      <span>Accepted</span>
    </Badge>
  )
  return <Badge color='warning' className='d-block'>
    <span>{status}</span>
  </Badge>
}

const renderLimitStop = (limit, stop) => {
  if (!limit && !stop) return '-'
  return `${limit || '...'} / ${stop || '...'}`
}

const renderTrail = (percent, price) => {
  if (!percent && !price) return '-'
  if (percent) return `${percent}%`
  if (price) return `$${price}`
}

const renderTif = (value) => {
  switch (value) {
    case 'day':
      return 'Day'
    default:
      return value.toUpperCase()
  }
}

const cancelHandler = (order_id) => {
  cancelOrder({
    order_id,
  }).then(() => {
    toast((t) => (
      <ToastContent
        t={t}
        name={'Success'}
        isError={false}
        message='Order successfully closed'
      />
    ))
  })

}

export const columns = [
  {
    name: 'Name',
    maxWidth: '30px',
    selector: row => row.symbol,
    cell: row => {
      return (
        <h6 className='d-flex justify-content-left align-items-center'>
          <Link to={{
            pathname: `/stocks/${row.symbol.toLowerCase()}`,
            search: "",
          }}>
            {row.symbol}
          </Link>
        </h6>
      )
    }
  },
  {
    name: 'ID',
    minWidth: '300px',
    selector: row => row.qty,
    cell: row => <div className='d-flex justify-content-left align-items-center text-nowrap small'>
      {row.id}
    </div>
  },
  {
    name: 'Side',
    maxWidth: '30px',
    selector: row => row.qty,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {renderSide(row.side)}
    </div>
  },
  {
    name: 'Quantity',
    maxWidth: '30px',
    selector: row => row.qty,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {row.qty}
    </div>
  },
  {
    name: 'Type',
    selector: row => row.role,
    cell: row => <p className='d-flex justify-content-left align-items-center'>
      {row.order_type.replace('_', ' ').toUpperCase()}
    </p>
  },
  {
    name: 'Limit / Stop',
    maxWidth: '30px',
    selector: row => row.qty,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {renderLimitStop(row.limit_price, row.stop_price)}
    </div>
  },
  {
    name: 'Trail',
    maxWidth: '30px',
    selector: row => row.qty,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {renderTrail(row.trail_percent, row.trail_price)}
    </div>
  },
  {
    name: 'Time in force',
    maxWidth: '30px',
    selector: row => row.qty,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {renderTif(row.time_in_force)}
    </div>
  },
  {
    name: 'Status',
    maxWidth: '30px',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {renderStatus(row.status)}
    </div>
  },
  {
    name: 'Time',
    maxWidth: '30px',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {row.created_at.split('T')[1].split('.')[0]}
    </div>
  },
  {
    name: 'Date',
    minWidth: '120px',
    maxWidth: '30px',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {row.created_at.split('T')[0]}
    </div>
  },
  {
    name: 'Date',
    minWidth: '180px',
    maxWidth: '30px',
    selector: row => row.role,
    cell: row => <Button.Ripple color='danger' onClick={() => {
      cancelHandler(row.id)
    }
    } size='sm' > Cancel</Button.Ripple >
  }
]