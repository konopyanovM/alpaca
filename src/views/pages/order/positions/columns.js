import { Badge, Button, Spinner } from 'reactstrap'
import { TrendingUp, TrendingDown, Minus, X } from 'react-feather'
import { createOrder, getStockBars } from '../../../../utility/Services'
import withReactContent from 'sweetalert2-react-content'
import Swal from 'sweetalert2'
import { useState } from 'react'
import { getStockDate } from '../../../../utility/Utils'
import { Link } from 'react-router-dom'
import toast from 'react-hot-toast'

const ToastContent = ({ t, name, isError, message, errMessage }) => {
  return (
    <div className="d-flex">
      <div className="d-flex flex-column">
        <div className="d-flex justify-content-between">
          <h6>{name}</h6>
          <X
            size={12}
            className="cursor-pointer"
            onClick={() => toast.dismiss(t.id)}
          />
        </div>
        {
          isError ? <span>
            {errMessage}
          </span> : <span>
            {message}
          </span>
        }

      </div>
    </div>
  )
}


const renderChange = (today, yesterday) => {
  const change = today / yesterday
  if (change < 1) {
    const diff = (1 - (today / yesterday)).toFixed(3)
    return <span className='text-danger fw-bold'>{diff}%</span>
  }
  if (change === 1) {
    const diff = 0
    return <span className='text-success fw-bold'>{diff}%</span>
  }
  if (change > 1) {
    const diff = ((yesterday / today)).toFixed(3)
    return <span className='text-success fw-bold'>{diff}%</span>
  }
}

const renderPosition = (type) => {
  if (type === 'long') return (
    <Badge color='success' className='d-block'>
      <span>LONG</span>
    </Badge>
  )
  if (type === 'short') return (
    <Badge color='danger' className='d-block'>
      <span>SHORT</span>
    </Badge>
  )
}

const renderPL = (pl) => {
  if (pl < 1) {
    return <h5 className='text-danger fw-bold fz-5'>{pl} $</h5>
  }
  if (pl === 1) {
    return <h5 className='fw-bold'>{pl} $</h5>
  }
  if (pl > 1) {
    return <h5 className='text-success fw-bold'>{pl} $</h5>
  }
}

const MySwal = withReactContent(Swal)

const onClosePosition = (symbol, qty, posSide) => {
  const side = posSide === 'long' ? 'sell' : 'buy'
  const params = {
    symbol,
    qty,
    side,
    type: 'market',
    time_in_force: 'day'
  }
  return async () => {
    return MySwal.fire({
      title: 'Are you sure ?',
      text: 'Are you sure you want to close the position?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Close',
      customClass: {
        confirmButton: 'btn btn-danger',
        cancelButton: 'btn btn-light ms-1'
      },
      buttonsStyling: false
    }).then(result => {
      if (result.value) {
        createOrder(params).then(() => {
          toast((t) => (
            <ToastContent
              t={t}
              name={'Success'}
              isError={false}
              message='Position successfully closed'
            />
          ))
        }).catch((err) => {
          toast((t) => (
            <ToastContent
              t={t}
              name={'Error'}
              isError={true}
              errMessage={err.response.data.message}
            />
          ))
        })
      } else if (result.dismiss === MySwal.DismissReason.cancel) {
        MySwal.fire({
          title: 'Cancelled',
          text: 'Close Cancelled!!',
          icon: 'error',
          customClass: {
            confirmButton: 'btn btn-success'
          }
        })
      }
    })
  }
}

export const columns = [
  {
    name: 'Name',
    compact: true,
    maxWidth: '30px',
    selector: row => row.symbol,
    cell: row => {
      return (
        <h6 className='d-flex justify-content-left align-items-center'>
          <Link to={{
            pathname: `/stocks/${row.symbol.toLowerCase()}`,
            search: "",
          }}>
            {row.symbol}
          </Link>
        </h6>
      )
    }
  },
  {
    name: 'ID',
    minWidth: '290px',
    maxWidth: '300px',
    selector: row => row.qty,
    cell: row => <div className='d-flex justify-content-left align-items-center text-nowrap small'>
      {row.asset_id}
    </div>
  },
  {
    name: 'Position',
    maxWidth: '30px',
    selector: row => row.qty,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {renderPosition(row.side)}
    </div>
  },
  {
    name: 'Exchange',
    maxWidth: '120px',
    selector: row => row.qty,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {row.exchange}
    </div>
  },
  {
    name: 'Cost basis',
    maxWidth: '30px',
    selector: row => row.qty,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {row.cost_basis}
    </div>
  },
  {
    name: 'Quantity',
    maxWidth: '30px',
    selector: row => row.qty,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {row.qty}
    </div>
  },
  {
    name: 'Entry price',
    maxWidth: '30px',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      ${(+row.avg_entry_price).toFixed(2)}
    </div>
  },
  {
    name: 'Current price',
    maxWidth: '30px',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      ${row.current_price}
    </div>
  },
  {
    name: 'Change (24h)',
    maxWidth: '30px',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {renderChange(row.market_value, row.lastday_price)}
    </div>
  },
  {
    name: 'Unrealized PL',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {renderPL(row.unrealized_pl)}
    </div>
  },
  {
    name: 'Action',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {/* {
        getMarket()[1]
          ? <Button className='me-1' color='danger' disabled={getMarket()[1]}>
            <Spinner size='sm' />
          </Button>
          : <Button onClick={onClosePosition(row.symbol, row.qty, row.side)} className='me-1' color='danger' disabled={getMarket()[0]}>
            Close
          </Button>
      } */}
      <Button onClick={onClosePosition(row.symbol, row.qty, row.side)} className='me-1 text-nowrap' color='danger'>
        Close
      </Button>
    </div>
  },
]