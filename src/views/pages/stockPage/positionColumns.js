import { Badge } from 'reactstrap'
import { Link } from 'react-router-dom'

const renderType = (type) => {
  if (type === 'long') return (
    <Badge color='success' className='d-block'>
      <span>{type.toUpperCase()}</span>
    </Badge>
  )
  if (type === 'short') return (
    <Badge color='danger' className='d-block'>
      <span>{type.toUpperCase()}</span>
    </Badge>
  )
}

const renderPL = (pl) => {
  if (pl < 1) {
    return <h5 className='text-danger fw-bold fz-5'>{pl} $</h5>
  }
  if (pl === 1) {
    return <h5 className='fw-bold'>{pl} $</h5>
  }
  if (pl > 1) {
    return <h5 className='text-success fw-bold'>{pl} $</h5>
  }
}

export const positionColumns = [
  {
    name: 'Name',
    selector: row => row.symbol,
    cell: row => {
      return (
        <h6 className='d-flex justify-content-left align-items-center'>
          <Link to={{
            pathname: `/stocks/${row.symbol.toLowerCase()}`,
            search: "",
          }}>
            {row.symbol}
          </Link>
        </h6>
      )
    }
  },
  {
    name: 'Side',
    selector: row => row.symbol,
    cell: row => {
      return (
        <div className='d-flex justify-content-left align-items-center'>
          {renderType(row.side)}
        </div>
      )
    }
  },
  {
    name: 'Quantity',
    maxWidth: '30px',
    selector: row => row.qty,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {row.qty}
    </div>
  },
  {
    name: 'Unr. PL',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {renderPL(row.unrealized_pl)}
    </div>
  }
]