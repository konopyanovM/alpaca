// ** Third Party Components
import Chart from 'react-apexcharts'

// ** Reactstrap Imports
import { Card, CardHeader, CardBody } from 'reactstrap'

// ** Third Party Components
import Select from 'react-select'

// ** Utils
import { selectThemeColors } from '@utils'


const BarChart = ({ danger, success, data = [], selectHandler }) => {
  const candle = {
    options: {
      xaxis: {
        // type: 'datetime',
        labels: {
          show: false
        }
      },
      yaxis: {
        tooltip: {
          enabled: true
        },
        opposite: 'rtl'
      },
      grid: {
        xaxis: {
          lines: {
            show: true
          }
        }
      },
      plotOptions: {
        candlestick: {
          colors: {
            upward: success,
            downward: danger
          }
        },
        bar: {
          columnWidth: '80%'
        }
      }
    },
    series: [
      {
        data: data[0]
      }
    ]
  }

  const volume = {
    series: [
      {
        name: 'Volume',
        data: data[1]
      }
    ],
    options: {
      chart: {
        toolbar: {
          show: false
        }
      },
      plotOptions: {
        bar: {
          borderRadius: 1,
          horizontal: false,
        }
      },
      xaxis: {
        type: 'datetime',
      },
      yaxis: {
        tooltip: {
          enabled: true
        },
        opposite: 'rtl'
      },
      grid: {
        xaxis: {
          lines: {
            show: true
          }
        }
      },
      dataLabels: {
        enabled: false
      },
      tooltip: {
        fixed: {
          enabled: true,
          position: 'topLeft', // topRight, topLeft, bottomRight, bottomLeft
          offsetY: -30,
          offsetX: 10
        },
      },
    },
  };

  const dataLength = data[0]?.length ? data[0].length - 1 : 0
  const startDate = data[0]?.length ? new Date(data[0][0].x).toISOString().split('T')[0] : '—'
  const endDate = data[0]?.length ? new Date(data[0][dataLength].x).toISOString().split('T')[0] : '—'

  const options = [
    { value: 30, label: '30d' },
    { value: 45, label: '45d' },
    { value: 60, label: '60d' },
    { value: 90, label: '90d' },
    { value: 180, label: '180d' },
  ]

  return (
    <Card className=''>
      <CardHeader className='d-flex flex-sm-row flex-column justify-content-md-between align-items-start justify-content-start'>
        <div className='w-100 mb-0 d-flex justify-content-between align-items-center'>
          <Select
            theme={selectThemeColors}
            className='react-select'
            classNamePrefix='select'
            defaultValue={options[2]}
            options={options}
            isClearable={false}
            onChange={(data) => { selectHandler(data.value) }}
          />
          <div>
            from <h6 className='d-inline-block'>{startDate}</h6> to <h6 className='d-inline-block'>{endDate}</h6>
          </div>
        </div>
        <div className='d-flex align-items-center mt-md-0 mt-1'>

        </div>
      </CardHeader>
      <CardBody>
        {data?.length === 0
          ? <h3 className='d-flex justify-content-center align-items-center pt-5'>Select an asset to view the chart</h3>
          : <>
            <Chart options={candle.options} series={candle.series} type="candlestick" height={400} />
            <Chart options={volume.options} series={volume.series} type="bar" height={200} />
          </>}
      </CardBody>
    </Card >
  )
}

export default BarChart

