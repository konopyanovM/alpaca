// ** Reactstrap Imports
import { Nav, NavItem, NavLink } from 'reactstrap'

// ** Icons Imports
import { Download, Eye } from 'react-feather'

const Tabs = ({ activeTab, toggleTab }) => {
  return (
    <Nav pills className='mb-2'>
      <NavItem>
        <NavLink active={activeTab === '1'} onClick={() => toggleTab('1')}>
          <Eye size={18} className='me-50' />
          <span className='fw-bold'>Open</span>
        </NavLink>
      </NavItem>
      <NavItem>
        <NavLink active={activeTab === '2'} onClick={() => toggleTab('2')}>
          <Download size={18} className='me-50' />
          <span className='fw-bold'>Download</span>
        </NavLink>
      </NavItem>
    </Nav>
  )
}

export default Tabs
