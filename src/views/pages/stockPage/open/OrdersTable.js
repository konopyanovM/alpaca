// ** Icons Imports
import { useState, useEffect } from 'react'
import { TrendingUp, TrendingDown, X } from 'react-feather'
import toast from 'react-hot-toast'
import { Button, Col, FormFeedback, Input, Table, Card, Modal, ModalBody, ModalHeader, Row, Label, Spinner } from 'reactstrap'
import { getDocumentInfo } from '../../../../utility/Services'

const OrdersTable = ({ data }) => {
  const [showModal, setShowModal] = useState(false)
  const [showSell, setShowSell] = useState(false)
  const [currentDocument, setCurrentDocument] = useState({})

  const ToastContent = ({ t, name, isError, message }) => {
    return (
      <div className="d-flex">
        <div className="d-flex flex-column">
          <div className="d-flex justify-content-between">
            <h6>{name}</h6>
            <X
              size={12}
              className="cursor-pointer"
              onClick={() => toast.dismiss(t.id)}
            />
          </div>
          {
            isError ? <span>
              Something went wrong.
            </span> : <span>
              {message}
            </span>
          }
        </div>
      </div>
    )
  }

  const renderDocData = (activities) => {
    if (!activities) return
    return activities.map((activity, index) => {
      return (
        <tr key={index}>
          <td>
            <div className='d-flex align-items-center'>
              <div>
                <div className='fw-bolder'>{activity.symbol}</div>
              </div>
            </div>
          </td>
          <td>
            <div className='d-flex align-items-center'>
              <div>
                <div className='fw-bolder'>{activity.side}</div>
              </div>
            </div>
          </td>
          <td className='flex-grow-1'>
            <div className='d-flex align-items-center'>
              <span>{activity.qty}</span>
            </div>
          </td>
          <td className='flex-grow-1'>
            <div className='d-flex align-items-center'>
              <span>{activity.price}</span>
            </div>
          </td>
          <td className='flex-grow-1'>
            <div className='d-flex align-items-center'>
              <span>{activity.gross_amount}</span>
            </div>
          </td>
          <td className='flex-grow-1'>
            <div className='d-flex align-items-center'>
              <span>{activity.net_amount}</span>
            </div>
          </td>
          <td className='flex-grow-1'>
            <div className='d-flex align-items-center'>
              <span>{activity.trade_date}</span>
            </div>
          </td>
          <td className='flex-grow-1'>
            <div className='d-flex align-items-center'>
              <span>{activity.trade_time}</span>
            </div>
          </td>
          <td className='flex-grow-1'>
            <div className='d-flex align-items-center'>
              <span>{activity.settle_date}</span>
            </div>
          </td>
          <td className='flex-grow-1'>
            <div className='d-flex align-items-center'>
              <span>{activity.asset_type}</span>
            </div>
          </td>
          <td className='flex-grow-1'>
            <div className='d-flex align-items-center'>
              <span>{activity.note}</span>
            </div>
          </td>
          <td className='flex-grow-1'>
            <div className='d-flex align-items-center'>
              <span>{activity.status}</span>
            </div>
          </td>
          <td className='flex-grow-1'>
            <div className='d-flex align-items-center'>
              <span>{activity.capacity}</span>
            </div>
          </td>
        </tr>
      )
    })
  }

  const renderDocFees = (activities) => {
    if (!activities || activities.length === 0) return (
      <tr>Not Found</tr>
    )
    return activities.map((activity, index) => {
      return (
        <tr key={index}>
          <td>
            <div className='d-flex align-items-center'>
              <div>
                <div className='fw-bolder'>{activity.symbol}</div>
              </div>
            </div>
          </td>
          <td>
            <div className='d-flex align-items-center'>
              <div>
                <div className='fw-bolder'>{activity.side}</div>
              </div>
            </div>
          </td>
          <td className='flex-grow-1'>
            <div className='d-flex align-items-center'>
              <span>{activity.qty}</span>
            </div>
          </td>
          <td className='flex-grow-1'>
            <div className='d-flex align-items-center'>
              <span>{activity.price}</span>
            </div>
          </td>
        </tr>
      )
    })
  }

  const renderData = () => {
    return data.data.map((item, index) => {
      return (
        <tr key={index}>
          <td>
            <div className='d-flex align-items-center'>
              <div>
                <div className='fw-bolder'>{item.id}</div>
              </div>
            </div>
          </td>
          <td>
            <div className='d-flex align-items-center'>
              <div className='avatar rounded'>
              </div>
              <div>
                <div className='fw-bolder'>{item.name}</div>
              </div>
            </div>
          </td>
          <td className='flex-grow-1'>
            <div className='d-flex align-items-center'>
              <span>{item.sub_type}</span>
            </div>
          </td>
          <td className='flex-grow-1'>
            <div className='d-flex align-items-center'>
              <span>{item.date}</span>
            </div>
          </td>
          <td>
            <Button.Ripple color='success' onClick={() => {
              setShowModal(!showModal)
              getDocumentInfo(item.id).then(res => {
                setCurrentDocument(res.data)
              })
            }}>Open</Button.Ripple>
          </td>
        </tr>
      )
    })
  }

  return (
    <Card className='company-table'>
      {
        data.data.length !== 0
          ? <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Sub_type</th>
                <th>Date</th>
                <th></th>
              </tr>
            </thead>
            <tbody>{renderData()}</tbody>
          </Table>
          : <div className='py-5 d-flex justify-content-center'>
            <Spinner />
          </div>
      }


      <Modal
        isOpen={showModal}
        toggle={() => setShowModal(!showModal)}
        className='modal-dialog-centered modal-xl'
      >
        <ModalHeader className='bg-transparent' toggle={() => setShowModal(!showModal)}></ModalHeader>
        <ModalBody className='px-sm-5 mx-50 pb-5 company-table'>
          <h3 className='text-center mb-2'>Document info</h3>
          <Row>
            <Col md="6"><p>Correspondent: <span className='fw-bold'>{currentDocument.correspondent}</span></p></Col>
            <Col md="6"><p>Account name: <span className='fw-bold'>{currentDocument.account_name}</span></p></Col>
          </Row>
          <Row>
            <Col md="6"><p>Representative: <span className='fw-bold'>{currentDocument.representative}</span></p></Col>
            <Col md="6"><p>Account No: <span className='fw-bold'>{currentDocument.account_no}</span></p></Col>
          </Row>
          <Row>
            <Col md="6"><p>Generated date: <span className='fw-bold'>{currentDocument.creation_date}</span></p></Col>
            <Col md="6"><p>Master Account No: <span className='fw-bold'>{currentDocument.master_account_no}</span></p></Col>
          </Row>
          <Col md="6"><h4>Trade activities</h4></Col>
          <Table>
            <thead>
              <tr>
                <th>Symbol</th>
                <th>Side</th>
                <th>Qty</th>
                <th>Price ({currentDocument.currency})</th>
                <th>Gross am. ({currentDocument.currency})</th>
                <th>Net am. ({currentDocument.currency})</th>
                <th>Trade date</th>
                <th>Trade time</th>
                <th>Settle date</th>
                <th>Asset type</th>
                <th>Note</th>
                <th>Status</th>
                <th>Capacity</th>
              </tr>
            </thead>
            <tbody>{renderDocData(currentDocument.trade_activities)}</tbody>
          </Table>
          <Col md="6"><h4>Fees activities</h4></Col>
          <Table>
            <thead>
              <tr>
                <th>Entry Type</th>
                <th>Gross am. ({currentDocument.currency})</th>
                <th>Description</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>{renderDocFees(currentDocument.fee_activities)}</tbody>
          </Table>
        </ModalBody>
      </Modal>
    </Card>
  )
}

export default OrdersTable
