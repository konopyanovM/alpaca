// ** React Imports
import { useState, useEffect } from 'react'

// ** Styles
import '@styles/react/libs/charts/apex-charts.scss'
import '@styles/base/pages/dashboard-ecommerce.scss'
import toast from 'react-hot-toast'
import { X } from 'react-feather'
import OrdersTable from './OrdersTable'
import { getDocuments } from '../../../../utility/Services'

const Orders = () => {
  const ToastContent = ({ t, name, isError, message }) => {
    return (
      <div className="d-flex">
        <div className="d-flex flex-column">
          <div className="d-flex justify-content-between">
            <h6>{name}</h6>
            <X
              size={12}
              className="cursor-pointer"
              onClick={() => toast.dismiss(t.id)}
            />
          </div>
          {
            isError ? <span>
            Something went wrong.
          </span> : <span>
            {message}
          </span>
          }
        </div>
      </div>
    )
  }

  const [data, setData] = useState([])

  const getData = async () => {
    const { data } = await getDocuments()
    const filteredData = data.documents.filter(item => item.type === 'trade_confirmation_json')
    setData(filteredData)
  }
  
  useEffect(()=>{
    getData()
  }, [])

  return (
          <OrdersTable data={{data}}></OrdersTable>
  )
}

export default Orders
