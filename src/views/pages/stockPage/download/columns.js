import { Badge, Button } from 'reactstrap'
import { TrendingUp, TrendingDown, Minus } from 'react-feather'
import { createOrder, downloadDocument, getDocuments } from '../../../../utility/Services'
import withReactContent from 'sweetalert2-react-content'
import Swal from 'sweetalert2'
import { useState } from 'react'

const renderType = (type) => {
  return (
    <Badge color='secondary' className='d-block'>
      <span>{type}</span>
    </Badge>
  )
}

export const columns = [
  {
    name: 'ID',
    selector: row => row.name,
    cell: row => {
      return (
        <div className='d-flex justify-content-left align-items-center'>
          {row.id}
        </div>
      )
    }
  },
  {
    name: 'Name',
    selector: row => row.name,
    cell: row => {
      return (
        <div className='d-flex justify-content-left align-items-center'>
          {row.name}
        </div>
      )
    }
  },
  {
    name: 'Subtype',
    selector: row => row.sub_type,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {row.sub_type}
    </div>
  },
  {
    name: 'Date',
    selector: row => row.date,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {row.date}
    </div>
  },
  {
    center: true,
    name: 'Action',
    selector: row => row.date,
    cell: row => {
      if (row.type === 'trade_confirmation') {
        return <Button color='info' onClick={() => {
          downloadDocument(row.id)
        }}>
          Download
        </Button>
      } else if (row.type === 'trade_confirmation_json') {
        return <>
          <Button color='success' onClick={() => {
            downloadDocument(row.id)
          }}>
            Open
          </Button>

        </>
      }
    }
  }
]