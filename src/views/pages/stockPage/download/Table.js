// ** React Imports
import { Fragment, useState, useEffect } from 'react'

// ** Table Columns
import { columns } from './columns'
import DataTable from 'react-data-table-component'
import { ChevronDown } from 'react-feather'

// ** Reactstrap Imports
import {
  Card, Spinner,
} from 'reactstrap'

// ** Styles
import '@styles/react/libs/react-select/_react-select.scss'
import '@styles/react/libs/tables/react-dataTable-component.scss'
import { getDocuments } from '../../../../utility/Services'

const UsersList = () => {
  const [list, setList] = useState([])

  const getPosition = async () => {
    const { data } = await getDocuments()
    const filteredData = data.documents.filter(item => item.type === 'trade_confirmation')
    setList(filteredData)
  }

  useEffect(() => {
    getPosition()
  }, [])

  return (
    <Fragment>
      <Card className='overflow-hidden'>

        {
          list.length !== 0
            ? <div className='react-dataTable'>
              <DataTable
                noHeader
                sortServer
                pagination={false}
                responsive
                paginationServer
                columns={columns}
                sortIcon={<ChevronDown />}
                className='react-dataTable'
                data={list}
              />
            </div>
            : <div className='py-5 d-flex justify-content-center'>
              <Spinner />
            </div>
        }
      </Card>
    </Fragment>
  )
}

export default UsersList
