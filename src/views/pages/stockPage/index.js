// ** User List Component
import OpenTable from './open'
import DownloadTable from './download/Table'
import { Button, Card, CardBody, Col, FormFeedback, Input, InputGroup, InputGroupText, Label, Nav, NavItem, NavLink, Row, Spinner, TabContent, TabPane } from 'reactstrap'
import { useState, useEffect, useContext } from 'react'
import { createOrder, getBalance, getLatestStockBar, getPositions, getStockBars, getUserOrders } from '../../../utility/Services'
import { Link, useNavigate, useParams } from 'react-router-dom'
import { Controller, useForm } from 'react-hook-form'
import toast from 'react-hot-toast'
import { ChevronDown, List, X } from 'react-feather'
import DataTable from 'react-data-table-component'
import { positionColumns } from './positionColumns'
import { orderColumns } from './orderColumns'
import { getStockDate } from '../../../utility/Utils'
import BarChart from './BarChart'
import Select from 'react-select'
import { selectThemeColors } from '@utils'
import { ThemeColors } from '@src/utility/context/ThemeColors'

const UsersList = () => {
  const defaultValues = {
    qty: '',
    limit_price: null,
    stop_price: null,
    trail_percent: null,
    trail_price: null,
    time_in_force: null,
  }

  const orderTypes = [
    { value: 'market', label: 'Market' },
    { value: 'limit', label: 'Limit' },
    { value: 'stop', label: 'Stop' },
    { value: 'stop_limit', label: 'Stop Limit' },
    { value: 'trailing_stop', label: 'Trailing Stop' },
  ]

  const timeInForce = [
    { value: 'day', label: 'Day' },
    { value: 'gtc', label: 'Good till Canceled' },
    { value: 'opg', label: 'At the Open' },
    { value: 'cls', label: 'At the Close' },
    { value: 'iok', label: 'Immediate or Cancel' },
    { value: 'fok', label: 'Fill or Kill' },
  ]

  const symbol = useParams().symbol

  // ** Theme Colors
  const { colors } = useContext(ThemeColors)
  // ** States
  const [stock, setStock] = useState(null)
  const [latestStock, setLatestStock] = useState(null)
  const [active, setActive] = useState('1')
  const [assetQty, setAssetQty] = useState(0)
  const [userPositions, setUserPositions] = useState(null)
  const [userBalance, setUserBalance] = useState(0)

  const [positionList, setPositionList] = useState(null)
  const [orderList, setOrderList] = useState(null)
  const [watchList, setWatchList] = useState([])

  const [stocks, setStocks] = useState(JSON.parse(localStorage.getItem('assets')) || [])
  const [chartData, setChartData] = useState(null)
  const [day, setDay] = useState(60)
  const [confirmBuy, setConfirmBuy] = useState(false)
  const [confirmSell, setConfirmSell] = useState(false)
  const [orderTypeValue, setOrderTypeValue] = useState({ value: 'market', label: 'Market' })
  const [timeInForceValue, setTimeInForceValue] = useState({ value: 'day', label: 'Day' })
  const [trailingStopValue, setTrailingStopValue] = useState('rate')
  const [currentSymbol, setCurrentSymbol] = useState(symbol || '')
  const [isUnknownAsset, setUnknownAsset] = useState(false)
  const [isFetching, setFetching] = useState(false)
  const [isExchangeClosed, setExchangeClosed] = useState(false)
  const {
    reset,
    control,
    setError,
    handleSubmit,
    clearErrors,
    watch,
    formState: { errors }
  } = useForm({ defaultValues })

  const toggle = tab => {
    if (active !== tab) {
      setActive(tab)
    }
  }

  const [start, end] = getStockDate()

  const ToastContent = ({ t, name, isError, message }) => {
    return (
      <div className="d-flex">
        <div className="d-flex flex-column">
          <div className="d-flex justify-content-between">
            <h6>{name}</h6>
            <X
              size={12}
              className="cursor-pointer"
              onClick={() => toast.dismiss(t.id)}
            />
          </div>
          {
            isError ? <span>
              Something went wrong.
            </span> : <span>
              {message}
            </span>
          }

        </div>
      </div>
    )
  }

  useEffect(() => {
    getPositions().then(res => {
      setUserPositions(res.data.results)
    })
  }, [])

  const setCurrentQty = (title) => {
    if (userPositions !== null) {
      const result = userPositions.filter((pos) => {
        return pos.symbol.toLowerCase() === title.toLowerCase()
      })[0]
      if (result) {
        setAssetQty(+result?.qty)
      } else {
        setAssetQty(0)
      }
    }
  }

  useEffect(() => {
    if (symbol) setCurrentQty(symbol)
  }, [userPositions, symbol])


  const getStockBar = async () => {
      const { data } = await getStockBars({
      symbol: symbol || 'AAPL',
      start,
      end,
    })
    if (data.message === 'Exchange closed') {
      setExchangeClosed(true)
    } else {
      setStock(data[Object.keys(data)[0]][1] || data[Object.keys(data)[0]][0])
    }
  }

  const setLatestStockBar = async () => {
    const { data } = await getLatestStockBar({
    symbol: symbol || 'AAPL',
  })
  setLatestStock(data)
}

  useEffect(() => {
    const array = []
    stocks.forEach((symbol) => {
      getStockBars({
        symbol,
        start,
        end,
      }).then((res) => {
        const keyName = Object.keys(res.data)[0]
        array.push(res.data[keyName])
      })
    })
    setWatchList(array)
  }, [stocks])

  const getChartData = async () => {
    if(symbol) {
    const [start, end] = getStockDate(day)
    const { data: rawData } = await getStockBars({
      symbol,
      start,
      end,
    })
    if (rawData && rawData.message !== 'Exchange closed') {
      const candleData = rawData[Object.keys(rawData)[0]].map((item) => {
        const date = new Date(item.timestamp)
        const { close, high, low, open } = item
        return { x: date, y: [open, high, low, close] }
      })
      const volumeData = rawData[Object.keys(rawData)[0]].map((item) => {
        const date = new Date(item.timestamp)
        const { volume } = item
        return { x: date, y: volume }
      })
      setChartData([candleData, volumeData])
    } else {
      setUnknownAsset(true)
    }
    }
  }

  const getPosition = async () => {
    const { data } = await getPositions()
    setPositionList(data.results)
  }

  const getOrders = async () => {
    const { data } = await getUserOrders()
    setOrderList(data.orders)
  }

  useEffect(() => {
    getBalance().then((res) => {
      setUserBalance(res.data.balance)
    })
    getStockBar()
    getPosition()
    setLatestStockBar()
  }, [])

  useEffect(() => {
    getOrders()
  }, [isFetching])

  useEffect(() => {
    getChartData()
  }, [day])

  const renderGainLoss = (high, low, open) => {
    const change = high / low
    if (change < 1) {
      const diff = (1 - (high / low)).toFixed(3)
      return <span> - <span className='text-danger fw-bold'>{(open - (open * diff)).toFixed(3)}$</span></span>
    }
    if (change === 1) {
      return <span className='text-success fw-bold'>~0</span>
    }
    if (change > 1) {
      const diff = ((low / high)).toFixed(3)
      return <span> + <span className='text-success fw-bold'>{(open - (open * diff)).toFixed(3)}$</span></span>
    }
  }

  const onComplete = (e) => {
    e.preventDefault()
    if (watch('qty').toString()) {
      clearErrors()
      setConfirmBuy(true)
      setConfirmSell(true)
    } else {
      setError('qty', {
        type: 'custom',
        message: 'Incorrect input',
      })
    }
  }

  const onCancel = (e) => {
    e.preventDefault()
    setConfirmBuy(false)
    setConfirmSell(false)
    clearErrors()
  }

  const onSubmitBuy = (data) => {
    if ((+data.qty * latestStock.open) > userBalance) {
      setError('qty', {
        type: 'custom',
        message: 'Insufficient funds'
      })
    } else if (+data.qty < 0) {
      setError('qty', {
        type: 'custom',
        message: 'Invalid input'
      })
    } else {
      const trail_price = trailingStopValue === 'rate' ? null : +data.trail_price
      const trail_percent = trailingStopValue === 'price' ? null : +data.trail_percent
      const params = {
        symbol: symbol.toUpperCase(),
        qty: +data.qty,
        side: 'buy',
        type: orderTypeValue.value,
        limit_price: data.limit_price,
        stop_price: data.stop_price,
        trail_price,
        trail_percent,
        time_in_force: timeInForceValue.value
      }
      setFetching(true)
      createOrder(params).then(() => {
        setConfirmBuy(false)
        setConfirmSell(false)
        setFetching(false)
        toast((t) => (
          <ToastContent
            t={t}
            name={'Success'}
            message='Buy order has been created'
          />
        ))
      }).catch((err) => {
        setConfirmBuy(false)
        setConfirmSell(false)
        setFetching(false)
        setError('qty', {
          type: 'custom',
          message: err.response.data.message,
        })
      })
    }
  }

  const onSubmitSell = (data) => {
    console.log(data);
    if (+data.qty > assetQty) {
      setError('qty', {
        type: 'custom',
        message: 'Not enough assets'
      })
    } else if (+data.qty < 0) {
      setError('qty', {
        type: 'custom',
        message: 'Invalid input'
      })
    } else {
      const trail_percent = trailingStopValue === 'rate' ? null : +data.trail_percent
      const trail_price = trailingStopValue === 'price' ? null : data.trail_price
      const params = {
        symbol: symbol.toUpperCase(),
        qty: +data.qty,
        side: 'sell',
        type: orderTypeValue.value,
        limit_price: data.limit_price,
        stop_price: data.stop_price,
        trail_price,
        trail_percent,
        time_in_force: timeInForceValue.value
      }
      setFetching(true)
      createOrder(params).then(() => {
        setConfirmSell(false)
        setConfirmBuy(false)
        setFetching(false)
        toast((t) => (
          <ToastContent
            t={t}
            name={'Success'}
            message='Sell order has been created'
          />
        ))
      }).catch((err) => {
        setConfirmSell(false)
        setConfirmBuy(false)
        setFetching(false)
        setError('qty', {
          type: 'custom',
          message: err.response.data.message,
        })
      })
    }
  }

  const renderOrderType = (value, side = 'buy') => {
    switch (value) {
      case 'limit':
        return <>
          <Col xs={12}>
            <h5 className='d-inline-block mb-1'>Limit price:</h5>
            <InputGroup className='input-group-merge'>
              <InputGroupText>$</InputGroupText>
              <Controller
                id="limit_price"
                name="limit_price"
                control={control}
                render={({ field }) => (
                  <Input
                    type='number'
                    invalid={errors.limit_price && true}
                    disabled={confirmBuy || confirmSell}
                    {...field}
                  />
                )}
              />
            </InputGroup>
          </Col>

          {errors.qty && <FormFeedback invalid>{errors.qty.message}</FormFeedback>}
        </>
      case 'stop':
        return <>
          <Col xs={12}>
            <h5 className='d-inline-block mb-1'>Stop price:</h5>
            <InputGroup className='input-group-merge'>
              <InputGroupText>$</InputGroupText>
              <Controller
                id="stop_price"
                name="stop_price"
                control={control}
                render={({ field }) => (
                  <Input
                    type='number'
                    invalid={errors.stop_price && true}
                    disabled={confirmBuy || confirmSell}
                    {...field}
                  />
                )}
              />
            </InputGroup>
          </Col>
        </>
      case 'stop_limit':
        return <>
          <Col xs={6}>
            <h5 className='d-inline-block mb-1'>Stop price:</h5>
            <InputGroup className='input-group-merge'>
              <InputGroupText>$</InputGroupText>
              <Controller
                id="stop_price"
                name="stop_price"
                control={control}
                render={({ field }) => (
                  <Input
                    type='number'
                    invalid={errors.stop_price && true}
                    disabled={confirmBuy || confirmSell}
                    {...field}
                  />
                )}
              />
            </InputGroup>
          </Col>
          <Col xs={6}>
            <h5 className='d-inline-block mb-1'>Limit price:</h5>
            <InputGroup className='input-group-merge'>
              <InputGroupText>$</InputGroupText>
              <Controller
                id="limit_price"
                name="limit_price"
                control={control}
                render={({ field }) => (
                  <Input
                    type='number'
                    invalid={errors.limit_price && true}
                    disabled={confirmBuy || confirmSell}
                    {...field}
                  />
                )}
              />
            </InputGroup>
          </Col>
        </>
      case 'trailing_stop':
        return <>
          <Col xs={12}>
            <h5 className='d-inline-block mb-1'>Trailing stop:</h5>
            <div className='d-flex gap-3 mb-2'>
              <div className='form-check'>
                <Input type='radio' name='ts_type' id='ts_type-rate' checked={trailingStopValue === 'rate'} onChange={() => {
                  setTrailingStopValue('rate')
                }} defaultChecked />
                <Label className='form-check-label' for='ts_type-rate'>
                  Trail rate
                </Label>
              </div>
              <div className='form-check'>
                <Input type='radio' name='ts_type' id='ts_type-price' checked={trailingStopValue === 'price'} onChange={() => {
                  setTrailingStopValue('price')
                }} />
                <Label className='form-check-label' for='ts_type-price'>
                  Trail price
                </Label>
              </div>
            </div>
            {
              trailingStopValue === 'rate'
                ? <InputGroup className='input-group-merge'>
                  <InputGroupText>%</InputGroupText>
                  <Controller
                    id="trail_percent"
                    name="trail_percent"
                    control={control}
                    render={({ field }) => (
                      <Input
                        type='number'
                        invalid={errors.trail_percent && true}
                        disabled={confirmBuy || confirmSell}
                        {...field}
                      />
                    )}
                  />
                </InputGroup>
                : <InputGroup className='input-group-merge'>
                  <InputGroupText>$</InputGroupText>
                  <Controller
                    id="trail_price"
                    name="trail_price"
                    control={control}
                    render={({ field }) => (
                      <Input
                        type='number'
                        invalid={errors.trail_price && true}
                        disabled={confirmBuy || confirmSell}
                        {...field}
                      />
                    )}
                  />
                </InputGroup>
            }
          </Col>
        </>
    }
  }

  const [timer, setTimer] = useState(null)

  const debounce = (callback, delay = 1000) => {
    return (arg) => {
      clearTimeout(timer)
      setTimer(setTimeout(() => {
        callback(arg)
      }, delay))
    }
  }

  const navigate = useNavigate();

  const navigateDebounce = debounce(navigate, 1000)

  const handleChange = (event) => {
    setCurrentSymbol(event.target.value)
    navigateDebounce(`/stocks/${event.target.value.toLowerCase()}`)
  }

  return (
    <Row>
      {isUnknownAsset
        ? <Col xs={12}>
          <Card color="danger">
            <CardBody>
              <h6 className='d-flex justify-content-center mb-0 text-white'>Unknown asset</h6>
            </CardBody>
          </Card>
        </Col>
        : null}
      {isExchangeClosed && !isUnknownAsset && !latestStock
        ? <Col xs={12} className="d-flex justify-content-center p-5 mt-5"><h1>Exchange is closed</h1></Col>
        : <>
          <Col xs={8}>
            {
              chartData !== null && latestStock !== null
                ?
                <>
                  <Card>
                    <CardBody>
                      <div className='d-flex justify-content-between'>
                        <div className='d-flex gap-4'>
                          <h1 className='display-4'>{latestStock.symbol}</h1>
                          <div>
                            <small className='d-inline-block mb-1'>Price and Change:</small>
                            <h4>{latestStock.open}${stock ? renderGainLoss(stock.high, stock.low, stock.open) : null}</h4>
                          </div>
                          <div>
                            <small className='d-inline-block mb-1'>Low:</small>
                            <h4>{latestStock.low}</h4>
                          </div>
                          <div className='me-5'>
                            <small className='d-inline-block mb-1'>High:</small>
                            <h4>{latestStock.high}</h4>
                          </div>
                        </div>
                        <div>
                          <div>
                            <small className='d-inline-block mb-1'>Date:</small>
                            <h4>{latestStock.timestamp.split('T')[0]}</h4>
                          </div>
                        </div>
                      </div>
                    </CardBody>
                  </Card>
                  <Card>
                    <CardBody>
                      <BarChart
                        data={chartData}
                        success={colors.success.main}
                        danger={colors.danger.main}
                        selectHandler={setDay}>
                      </BarChart>
                    </CardBody>
                  </Card>
                </>
                : currentSymbol === ''
                  ? <>
                    <Card>
                      <CardBody>
                        <div className='d-flex justify-content-between'>
                          <div className='d-flex gap-4'>
                            <h1 className='display-4'>—</h1>
                            <div>
                              <small className='d-inline-block mb-1'>Price and Change:</small>
                              <h4>— $</h4>
                            </div>
                            <div>
                              <small className='d-inline-block mb-1'>Low:</small>
                              <h4>—</h4>
                            </div>
                            <div className='me-5'>
                              <small className='d-inline-block mb-1'>High:</small>
                              <h4>—</h4>
                            </div>
                          </div>
                          <div>
                            <div>
                              <small className='d-inline-block mb-1'>Date:</small>
                              <h4>—</h4>
                            </div>
                          </div>
                        </div>
                      </CardBody>
                    </Card>
                    <Card>
                      <CardBody className='' style={{ height: 565 }}>
                        <BarChart
                          data={[]}
                          success={colors.success.main}
                          danger={colors.danger.main}
                          selectHandler={setDay}>
                        </BarChart>
                      </CardBody>
                    </Card>
                  </>
                  : <div className='py-5 d-flex justify-content-center'>
                    <Spinner />
                  </div>
            }
            <Card>
              <CardBody>
                {orderList !== null
                  ? <>
                    <h5 className='d-flex justify-content-between'>
                      <span>Orders</span>
                      <Link to={{
                        pathname: '/orders-positions',
                        search: "",
                      }}>
                        <List></List>
                      </Link>
                    </h5>
                    <div className='react-dataTable'>
                      <DataTable
                        noHeader
                        sortServer
                        responsive
                        pagination={false}
                        paginationServer
                        columns={orderColumns}
                        sortIcon={<ChevronDown />}
                        className='react-dataTable'
                        data={orderList.slice(0, 5)}
                      />
                    </div>
                    {orderList.length > 5
                      ? <Link to={{
                        pathname: '/orders-positions',
                        search: "",
                      }}>
                        <h6 className='w-100 d-flex justify-content-center'>Show all...</h6>
                      </Link>
                      : null}
                  </>
                  : <div className='py-5 d-flex justify-content-center'>
                    <Spinner />
                  </div>
                }
              </CardBody>
            </Card>
          </Col>
          <Col xs={4}>
            <Card>
              <CardBody>
                <h6 className='d-flex justify-content-between mb-0'><span>Quantity:</span><span>{assetQty}</span></h6>
              </CardBody>
            </Card>
            <Card>
              <CardBody>
                <Nav tabs justified>
                  <NavItem>
                    <NavLink
                      active={active === '1'}
                      onClick={() => {
                        toggle('1')
                      }}
                      className="buy-tab"
                      disabled={confirmBuy || confirmSell}
                    >
                      Buy
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      active={active === '2'}
                      onClick={() => {
                        toggle('2')
                      }}
                      className="sell-tab"
                      disabled={confirmBuy || confirmSell}
                    >
                      Sell
                    </NavLink>
                  </NavItem>
                </Nav>
                <TabContent className='py-50' activeTab={active}>
                  <TabPane tabId='1'>
                    <form onSubmit={handleSubmit(onSubmitBuy)}>
                      <Row className='mb-1'>
                        <Col xs={6}>
                          <h5 className='d-inline-block mb-1'>Symbol:</h5>
                          <Input onChange={handleChange} disabled={confirmBuy} value={currentSymbol.toUpperCase()}></Input>
                        </Col>
                        <Col xs={6}>
                          <h5 className='d-inline-block mb-1'>Market price:</h5>
                          <h6 className='fw-normal'>{latestStock?.open || 0}</h6>
                        </Col>
                      </Row>
                      <Row className='mb-1'>
                        <Col xs={6}>
                          <h5 className='d-inline-block mb-1'>Order type:</h5>
                          <Select
                            theme={selectThemeColors}
                            className='react-select'
                            classNamePrefix='select'
                            value={orderTypeValue}
                            options={orderTypes}
                            isClearable={false}
                            isDisabled={confirmBuy || !currentSymbol}
                            onChange={(data) => { setOrderTypeValue(data) }}
                          />
                        </Col>
                        <Col xs={6}>
                          <h5 className='d-inline-block mb-1'>Time in force:</h5>
                          <Select
                            theme={selectThemeColors}
                            className='react-select'
                            classNamePrefix='select'
                            value={timeInForceValue}
                            options={timeInForce}
                            isClearable={false}
                            isDisabled={confirmBuy || !currentSymbol}
                            onChange={(data) => { setTimeInForceValue(data) }}
                          />
                        </Col>
                      </Row>
                      <Row className='mb-1'>
                        {renderOrderType(orderTypeValue.value)}
                      </Row>
                      <Row>
                        <Col xs={6}>
                          <div className="mb-1">
                            <div className="d-flex justify-content-between">
                              <h5 className='d-inline-block mb-1'>Quantity:</h5>
                            </div>
                            <Controller
                              id="qty"
                              name="qty"
                              control={control}
                              render={({ field }) => (
                                <Input
                                  type='number'
                                  className="input-group-merge"
                                  invalid={errors.qty && true}
                                  disabled={confirmBuy || !currentSymbol}
                                  {...field}
                                />
                              )}
                            />
                            {errors.qty && <FormFeedback invalid>{errors.qty.message}</FormFeedback>}
                          </div>
                        </Col>
                        <Col xs={6}>
                          <h5 className='d-inline-block mb-1'>Estimated price:</h5>
                          <h6>{watch('qty') ? `${(((latestStock?.open || 0) * watch('qty')).toFixed(3))}$` : '0.000$'}</h6>
                        </Col>
                      </Row>
                      <Row className='mb-1'>
                        {
                          !confirmBuy
                            ? <Col className='text-center mt-1' xs={12}>
                              <Button className='me-1 w-100' color='success' onClick={onComplete} disabled={!currentSymbol}>
                                Buy
                              </Button>
                            </Col>
                            : <>
                              <Col className='text-center mt-1' xs={12}>
                                <p>Are you sure you want to place this order? <span className='fw-bold text-white'>Buy for {watch('qty')} of {latestStock.symbol}&nbsp;|&nbsp;${latestStock.open} / share.</span> If so, press <span className='fw-bold text-white'>Confirm</span> to place your trade.</p>
                              </Col>
                              <Col className='text-center mt-1' xs={6} onClick={onCancel}>
                                <Button className='me-1 w-100' color='light' >
                                  Cancel
                                </Button>
                              </Col>
                              <Col className='text-center mt-1' xs={6}>
                                <Button type='submit' className='me-1 w-100' color='success' disabled={isFetching}>
                                  {!isFetching ? 'Confirm' : <Spinner size='sm' />}
                                </Button>
                              </Col>
                            </>
                        }
                      </Row>
                    </form>
                  </TabPane>
                  <TabPane tabId='2'>
                    <form onSubmit={handleSubmit(onSubmitSell)}>
                      <Row className='mb-1'>
                        <Col xs={6}>
                          <h5 className='d-inline-block mb-1'>Symbol:</h5>
                          <Input onChange={handleChange} disabled={confirmBuy} value={currentSymbol.toUpperCase()}></Input>
                        </Col>
                        <Col xs={6}>
                          <h5 className='d-inline-block mb-1'>Market price:</h5>
                          <h6 className='fw-normal'>{latestStock?.open || 0}</h6>
                        </Col>
                      </Row>
                      <Row className='mb-1'>
                        <Col xs={6}>
                          <h5 className='d-inline-block mb-1'>Order type:</h5>
                          <Select
                            theme={selectThemeColors}
                            className='react-select'
                            classNamePrefix='select'
                            value={orderTypeValue}
                            options={orderTypes}
                            isClearable={false}
                            isDisabled={confirmSell || !currentSymbol}
                            onChange={(data) => { setOrderTypeValue(data) }}
                          />
                        </Col>
                        <Col xs={6}>
                          <h5 className='d-inline-block mb-1'>Time in force:</h5>
                          <Select
                            theme={selectThemeColors}
                            className='react-select'
                            classNamePrefix='select'
                            value={timeInForceValue}
                            options={timeInForce}
                            isClearable={false}
                            isDisabled={confirmSell || !currentSymbol}
                            onChange={(data) => { setTimeInForceValue(data) }}
                          />
                        </Col>
                      </Row>
                      <Row className='mb-1'>
                        {renderOrderType(orderTypeValue.value)}
                      </Row>
                      <Row>
                        <Col xs={6}>
                          <div className="mb-1">
                            <div className="d-flex justify-content-between">
                              <h5 className='d-inline-block mb-1'>Quantity:</h5>
                            </div>
                            <Controller
                              id="qty"
                              name="qty"
                              control={control}
                              render={({ field }) => (
                                <Input
                                  type='number'
                                  className="input-group-merge"
                                  invalid={errors.qty && true}
                                  disabled={confirmSell || !currentSymbol}
                                  {...field}
                                />
                              )}
                            />
                            {errors.qty && <FormFeedback invalid>{errors.qty.message}</FormFeedback>}
                          </div>
                        </Col>
                        <Col xs={6}>
                          <h5 className='d-inline-block mb-1'>Estimated price:</h5>
                          <h6>{watch('qty') ? `${(((latestStock?.open || 0) * watch('qty')).toFixed(3))}$` : '0.000$'}</h6>
                        </Col>
                      </Row>
                      <Row className='mb-1'>
                        {
                          !confirmSell
                            ? <Col className='text-center mt-1' xs={12}>
                              <Button type='submit' className='me-1 w-100' color='danger' onClick={onComplete} disabled={!currentSymbol}>
                                Sell
                              </Button>
                            </Col>
                            : <>
                              <Col className='text-center mt-1' xs={12}>
                                <p>Are you sure you want to place this order? <span className='fw-bold text-white'>Sell for {watch('qty')} of {latestStock.symbol}&nbsp;|&nbsp;${latestStock.open} / share.</span> If so, press <span className='fw-bold text-white'>Confirm</span> to place your trade.</p>
                              </Col>
                              <Col className='text-center mt-1' xs={6} onClick={onCancel}>
                                <Button className='me-1 w-100' color='light' >
                                  Cancel
                                </Button>
                              </Col>
                              <Col className='text-center mt-1' xs={6}>
                                <Button type='submit' className='me-1 w-100' color='success' disabled={isFetching}>
                                  {!isFetching ? 'Confirm' : <Spinner size='sm' />}
                                </Button>
                              </Col>
                            </>
                        }
                      </Row>
                    </form>
                  </TabPane>
                </TabContent>
              </CardBody>
            </Card>
            <Card>
              <CardBody>
                {positionList !== null
                  ? <>
                    <h5 className='d-flex justify-content-between'>
                      <span>Positions</span>
                      <Link to={{
                        pathname: '/orders-positions',
                        search: "tab=2",
                      }}>
                        <List></List>
                      </Link>
                    </h5>
                    <div className='react-dataTable table-width'>
                      <DataTable
                        noHeader
                        sortServer
                        responsive
                        pagination={false}
                        paginationServer
                        columns={positionColumns}
                        sortIcon={<ChevronDown />}
                        className='react-dataTable'
                        data={positionList.slice(0, 5)}
                      />
                    </div>
                    {positionList.length > 5
                      ? <Link to={{
                        pathname: '/orders-positions',
                        search: "",
                      }}>
                        <h6 className='w-100 d-flex justify-content-center'>Show all...</h6>
                      </Link>
                      : null}
                  </>
                  : <div className='py-5 d-flex justify-content-center'>
                    <Spinner />
                  </div>
                }
              </CardBody>
            </Card>
          </Col>
        </>}
    </Row >
  )
}

export default UsersList
