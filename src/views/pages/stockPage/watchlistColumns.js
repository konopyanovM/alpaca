import { Link } from 'react-router-dom'
import { Badge } from 'reactstrap'

const renderSide = (side) => {
  if (side === 'buy') return (
    <Badge color='success' className='d-block'>
      <span>Buy</span>
    </Badge>
  )
  if (side === 'sell') return (
    <Badge color='danger' className='d-block'>
      <span>Sell</span>
    </Badge>
  )
}

const renderStatus = (status) => {
  if (status === 'accepted') return (
    <Badge color='success' className='d-block'>
      <span>Accepted</span>
    </Badge>
  )
  return <Badge color='warning' className='d-block'>
    <span>{status}</span>
  </Badge>
}

const renderLimitStop = (limit, stop) => {
  if (!limit && !stop) return '-'
  return `${limit || '...'} / ${stop || '...'}`
}

const renderTrail = (percent, price) => {
  if (!percent && !price) return '-'
  if (percent) return `${percent}%`
  if (price) return `$${price}`
}

export const orderColumns = [
  {
    name: 'Name',
    selector: row => row.symbol,
    cell: row => {
      return (
        <h6 className='d-flex justify-content-left align-items-center'>
          <Link to={{
            pathname: `/stocks/${row.symbol.toLowerCase()}`,
            search: "",
          }}>
            {row.symbol}
          </Link>
        </h6>
      )
    }
  },
  {
    name: 'Side',
    selector: row => row.qty,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {renderSide(row.side)}
    </div>
  },
  {
    name: 'Type',
    selector: row => row.role,
    cell: row => <h6 className='d-flex justify-content-left align-items-center'>
      {row.order_type.replace('_', ' ').toUpperCase()}
    </h6>
  },
  {
    name: 'Quantity',
    selector: row => row.qty,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {row.qty}
    </div>
  },
  {
    name: 'Limit / Stop',
    selector: row => row.qty,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {renderLimitStop(row.limit_price, row.stop_price)}
    </div>
  },
  {
    name: 'Trail',
    selector: row => row.qty,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {renderTrail(row.trail_percent, row.trail_price)}
    </div>
  },
  {
    name: 'Status',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {renderStatus(row.status)}
    </div>
  },
  {
    name: 'Date',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {row.created_at.split('T')[0]}
    </div>
  },
  {
    name: 'Time',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {row.created_at.split('T')[1].split('.')[0]}
    </div>
  }
]