// ** React Imports
// import { useContext } from 'react'
import { Link, useNavigate } from 'react-router-dom'

// ** Custom Hooks
import { useSkin } from '@hooks/useSkin'
import useJwt from '@src/auth/jwt/useJwt'
import themeConfig from '@configs/themeConfig'

// ** Third Party Components
import toast from 'react-hot-toast'
import { useDispatch } from 'react-redux'
import { useForm, Controller } from 'react-hook-form'
import {
  Coffee,
  X
} from 'react-feather'

// ** Context
import { AbilityContext } from '@src/utility/context/Can'

// ** Actions
import { handleLogin } from '@store/authentication'

// ** Context
// import { AbilityContext } from '@src/utility/context/Can'

// ** Custom Components
import Avatar from '@components/avatar'
import InputPasswordToggle from '@components/input-password-toggle'

// ** Utils
import { getHomeRouteForLoggedInUser } from '@utils'

// ** Reactstrap Imports
import {
  Row,
  Col,
  Form,
  Input,
  Label,
  Button,
  CardTitle,
} from 'reactstrap'

// ** Styles
import '@styles/react/pages/page-authentication.scss'
import axios from 'axios'
import { getUserDetail } from '../../../utility/Services'
import { useContext } from 'react'

const ToastContent = ({ t, name, role }) => {
  return (
    <div className="d-flex">
      <div className="me-1">
        <Avatar size="sm" color="success" icon={<Coffee size={12} />} />
      </div>
      <div className="d-flex flex-column">
        <div className="d-flex justify-content-between">
          <h6>{name}</h6>
          <X
            size={12}
            className="cursor-pointer"
            onClick={() => toast.dismiss(t.id)}
          />
        </div>
        <span>
          You have successfully logged in as an {role} user. Now you
          can start to trade!
        </span>
      </div>
    </div>
  )
}

const defaultValues = {
  email: '',
  password: ''
}

const Login = () => {
  // ** Hooks
  const { skin } = useSkin()
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const abilityContext = useContext(AbilityContext)

  const {
    control,
    setError,
    handleSubmit,
    formState: { errors }
  } = useForm({ defaultValues })
  const illustration = skin === 'dark' ? 'login-v2-dark.svg' : 'login-v2.svg',
    source = require(`@src/assets/images/pages/${illustration}`).default

  const onSubmit = (data) => {
    if (Object.values(data).every((field) => field.length > 0)) {
      useJwt
        .login({ email: data.email, password: data.password })
        .then((res) => {
          const accessToken = res.data.tokens.access
          const refreshToken = res.data.tokens.refresh
          axios.defaults.headers.common[
            'Authorization'
          ] = `Bearer ${accessToken}`
          getUserDetail().then((res) => {
            const userDetail = res.data
            let ability = []
            let role
            if (userDetail.is_superuser) {
              ability = [
                {
                  action: 'manage',
                  subject: 'all'
                }
              ]
              role = 'admin'
            } else {
              ability = [
                {
                  action: 'read',
                  subject: 'all'
                },
                {
                  action: 'read',
                  subject: 'all'
                }
              ]
              role = 'client'
            }
            const data = {
              ...userDetail,
              accessToken,
              refreshToken,
              ability,
              role
            }
            dispatch(handleLogin(data))
            abilityContext.update(ability)
            navigate(getHomeRouteForLoggedInUser(data.role))
            toast((t) => (
              <ToastContent
                t={t}
                role={data.role || 'admin'}
                name={data.first_name || data.username || 'John Doe'}
              />
            ))
          })
        })
        .catch(() => {
          setError("email", {
            type: 'manual',
            message: 'Incorrect email or password'
          })
          setError('password', {
            type: 'manual'
          })
        })
    } else {
      for (const key in data) {
        if (data[key].length === 0) {
          setError(key, {
            type: 'manual',
          })
        }
      }
    }
  }

  return (
    <div className="auth-wrapper auth-cover">
      <Row className="auth-inner m-0">
        <Link className='brand-logo align-items-center' to='/'>
          <img src={themeConfig.app.appLogoImage} alt='logo' width={50} />
          <h2 className='brand-text text-primary mb-0 pl-1 mx-1'>{themeConfig.app.appName}</h2>
        </Link>
        <Col className="d-none d-lg-flex align-items-center p-5" lg="8" sm="12">
          <div className="w-100 d-lg-flex align-items-center justify-content-center px-5">
            <img className="img-fluid" src={source} alt="Login Cover" />
          </div>
        </Col>
        <Col
          className="d-flex align-items-center auth-bg px-2 p-lg-5"
          lg="4"
          sm="12"
        >
          <Col className="px-xl-2 mx-auto" sm="8" md="6" lg="12">
            <CardTitle tag="h2" className="fw-bold mb-1">
              Log in to continue
            </CardTitle>
            <Form
              className="auth-login-form mt-2"
              onSubmit={handleSubmit(onSubmit)}
            >
              {errors.email ? <p className='alert' style={{ color: '#ea5455' }}>{errors.email.message}</p> : null}
              <div className="mb-1">
                <Label className="form-label" for="login-email">
                  Email
                </Label>
                <Controller
                  id="email"
                  name="email"
                  control={control}
                  render={({ field }) => (
                    <Input
                      tabIndex={1}
                      autoFocus
                      type="email"
                      placeholder="john@example.com"
                      invalid={errors.email && true}
                      {...field}
                    />
                  )}
                />
              </div>
              <div className="mb-1">
                <div className="d-flex justify-content-between">
                  <Label className="form-label" for="login-password">
                    Password
                  </Label>
                </div>
                <Controller
                  id="password"
                  name="password"
                  control={control}
                  render={({ field }) => (
                    <InputPasswordToggle
                      tabIndex={2}
                      className="input-group-merge"
                      invalid={errors.password && true}
                      {...field}
                    />
                  )}
                />
              </div>
              <Button type="submit" color="primary" block>
                Sign in
              </Button>
            </Form>
            <p className="text-center mt-2">
              <span className="me-25">New on our platform?</span>
              <Link to="/register">
                <span>Create an account</span>
              </Link>
            </p>
          </Col>
        </Col>
      </Row>
    </div>
  )
}

export default Login
