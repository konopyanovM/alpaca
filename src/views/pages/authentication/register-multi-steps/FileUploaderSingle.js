// ** React Imports
import { useState, Fragment, useEffect } from 'react'

// ** Reactstrap Imports
import { Card, CardHeader, CardTitle, CardBody, Button, ListGroup, ListGroupItem, Badge } from 'reactstrap'

// ** Third Party Imports
import { useDropzone } from 'react-dropzone'
import { FileText, X, DownloadCloud, Check } from 'react-feather'

const FileUploaderSingle = ({ title, data, setData, isValid, fileTypes }) => {
  // ** State
  const [files, setFiles] = useState([])
  const [typesValid, setTypesValid] = useState(true)

  const { getRootProps, getInputProps } = useDropzone({
    multiple: false,
    onDrop: acceptedFiles => {
      if (fileTypes.includes(acceptedFiles[0].type)) {
        setTypesValid(true)
        setFiles([...files, ...acceptedFiles.map(file => Object.assign(file))])
      } else {
        setTypesValid(false)
      }

    }
  })

  const renderFilePreview = file => {
    if (file.type.startsWith('image')) {
      return <img className='rounded' alt={file.name} src={URL.createObjectURL(file)} height='28' width='28' />
    } else {
      return <FileText size='28' />
    }
  }

  const handleRemoveFile = file => {
    const uploadedFiles = files
    const filtered = uploadedFiles.filter(i => i.name !== file.name)
    setFiles([...filtered])
  }

  const renderFileSize = size => {
    if (Math.round(size / 100) / 10 > 1000) {
      return `${(Math.round(size / 100) / 10000).toFixed(1)} mb`
    } else {
      return `${(Math.round(size / 100) / 10).toFixed(1)} kb`
    }
  }

  const renderFileType = (fileType) => {
    switch (fileType) {
      case 'image/jpeg' || 'image/jpg':
        return <span className='align-middle ms-25'>.jpeg</span>
      case 'image/png':
        return <span className='align-middle ms-25'>.png</span>
      case 'application/pdf':
        return <span className='align-middle ms-25'>.pdf</span>
      case 'application/json':
        return <span className='align-middle ms-25'>.json</span>
    }
  }

  useEffect(() => {
    setData(() => files)
  }, [files])

  const fileList = files.map((file, index) => (
    <ListGroupItem key={`${file.name}-${index}`} className='d-flex align-items-center justify-content-between'>
      <div className='file-details d-flex align-items-center'>
        <div className='file-preview me-1'>{renderFilePreview(file)}</div>
        <div>
          <p className='file-name mb-0'>{file.name}</p>
          <p className='file-size mb-0'>{renderFileSize(file.size)}</p>
        </div>
      </div>
      <Button color='danger' outline size='sm' className='btn-icon' onClick={() => handleRemoveFile(file)}>
        <X size={14} />
      </Button>
    </ListGroupItem>
  ))

  const handleRemoveAllFiles = () => {
    setFiles([])
  }

  const handleUpload = () => {
  }

  return (
    <Card className='mb-0'>
      {title ?
        <CardHeader>
          <CardTitle tag='h4'>{title}</CardTitle>
        </CardHeader>
        : null
      }

      <CardBody>
        {data !== null && data?.length === 0 ?
          <div {...getRootProps({ className: `dropzone ${(isValid === null || isValid) || 'border-danger'} ${(typesValid) || 'border-danger'}` })}>
            <input {...getInputProps()} />
            <div className='d-flex align-items-center justify-content-center flex-column'>
              <DownloadCloud size={64} />
            </div>
          </div>
          : null
        }

        {files.length ? (
          <Fragment>
            <ListGroup className='my-2'>{fileList}</ListGroup>
          </Fragment>
        ) : null}
        <div className='mt-2 mb-1 d-flex gap-1'>

        </div>
        {typesValid || <small className='mx-1 text-danger d-flex flex-column'><span>Invalid file extension, only</span> <span className='d-flex gap-1'>
          {
              fileTypes?.map((fileType) => {
                return renderFileType(fileType)
              })
            }
        </span></small>}
      </CardBody>
    </Card>
  )
}

export default FileUploaderSingle
