// ** React Imports
import { Link } from 'react-router-dom'
import { useRef, useState } from 'react'

// ** Custom Components
import Wizard from '@components/wizard'
import themeConfig from '@configs/themeConfig'
// ** Reactstrap Imports
import { Row, Col } from 'reactstrap'

// ** Third Party Components
import { CreditCard, Eye, File, Home, Maximize, User } from 'react-feather'

// ** Steps
import BrokerRequirements from './steps/BrokerRequirements'
import TaxRequirements from './steps/TaxRequirements'
import CustomerProfile from './steps/CustomerProfile'
import FinancialProfileAndAffiliations from './steps/FinancialProfileAndAffiliations'
import IdentityVerifications from './steps/IdentityVerification'

// ** Styles
import '@styles/react/pages/page-authentication.scss'
import '@styles/react/libs/react-select/_react-select.scss'
import Onfido from './steps/Onfido'

const RegisterMultiSteps = () => {
  // ** Ref
  const ref = useRef(null)

  // ** State
  const [stepper, setStepper] = useState(null)

  const [formData, setFormData] = useState()

  const steps = [
    {
      id: 'account-details',
      title: 'Customer profile',
      subtitle: 'Enter personal information',
      icon: <User size={18} />,
      content: <CustomerProfile stepper={stepper} setFormData={setFormData}/>
    },
    {
      id: 'personal-info',
      title: 'Financial profile',
      subtitle: 'Enter financial information',
      icon: <CreditCard size={18} />,
      content: <FinancialProfileAndAffiliations stepper={stepper} setFormData={setFormData}/>
    },
    {
      id: 'identity-verification',
      title: 'Identity verification',
      subtitle: 'Verify your identity',
      icon: <Eye size={18} />,
      content: <IdentityVerifications stepper={stepper} setFormData={setFormData}/>
    },
    {
      id: 'onfido',
      title: 'Onfido verification',
      subtitle: '',
      icon: <Maximize size={18} />,
      content: <Onfido stepper={stepper} setFormData={setFormData} formData={formData}/>
    },
    {
      id: 'tax-info',
      title: 'Tax requirements',
      subtitle: 'Enter tax information',
      icon: <File size={18} />,
      content: <TaxRequirements stepper={stepper} setFormData={setFormData}/>
    },
    {
      id: 'broker-info',
      title: 'Broker requirements',
      subtitle: 'Enter broker information',
      icon: <File size={18} />,
      content: <BrokerRequirements stepper={stepper} formData={formData}/>
    },
  ]

  const source = require('@src/assets/images/pages/create-account.svg').default

  return (
    <div className='auth-wrapper auth-cover'>
      <Row className='auth-inner m-0'>
        <Link className='brand-logo align-items-center' to='/'>
            <img src={themeConfig.app.appLogoImage} alt='logo' width={50}/>
            <h2 className='brand-text text-primary mb-0 pl-1 mx-1'>{themeConfig.app.appName}</h2>
        </Link>
        <Col lg='3' className='d-none d-lg-flex align-items-center p-0'>
          <div className='w-100 d-lg-flex align-items-center justify-content-center'>
            <img className='img-fluid w-100' src={source} alt='Login Cover' />
          </div>
        </Col>
        <Col lg='9' className='d-flex align-items-center auth-bg px-2 px-sm-3 px-lg-5 pt-3'>
          <div className='width-800 mx-auto'>
            <Wizard
              ref={ref}
              steps={steps}
              instance={el => setStepper(el)}
              headerClassName='px-0'
              contentWrapperClassName='px-0'
              className='register-multi-steps-wizard shadow-none'
            />
          </div>
        </Col>
      </Row>
    </div>
  )
}

export default RegisterMultiSteps
