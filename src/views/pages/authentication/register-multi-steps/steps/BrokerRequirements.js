// ** React Imports
import { Fragment, useState } from 'react'
import useJwt from '@src/auth/jwt/useJwt'

import { useForm, Controller } from 'react-hook-form'
import { ChevronLeft, Check, X } from 'react-feather'
// ** Reactstrap Imports
import { Form, Label, Input, Row, Col, Button, InputGroup, InputGroupText, FormFeedback, Spinner } from 'reactstrap'

import { useNavigate } from 'react-router-dom'
import toast from 'react-hot-toast'
import FileUploaderSingle from '../FileUploaderSingle'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'

const onfidoToken = "onfido-jwt";
const onfidoContainerId = "onfido-sdk-wrapper";

const BrokerRequirements = ({ stepper, formData }) => {
  // ** States
  const {
    control,
    setError,
    handleSubmit,
    formState: { errors }
  } = useForm({
    defaultValues: {
      agreement: '',
      crypto_agreement: '',
      notUs: '',
      signature: '',
    }
  })
  const [isFetching, setFetching] = useState(false)

  const ToastContent = ({ t, name, isError, message }) => {
    return (
      <div className="d-flex">
        <div className="d-flex flex-column">
          <div className="d-flex justify-content-between">
            <h6>{name}</h6>
            <X
              size={12}
              className="cursor-pointer"
              onClick={() => toast.dismiss(t.id)}
            />
          </div>
          {
            isError ? <span>
              {message}
            </span> : <span>
              {message}
            </span>
          }

        </div>
      </div>
    )
  }

  const navigate = useNavigate()

  const onSubmit = data => {
    if(!data.agreement) {
      setError('agreement', {type: 'manual'})
    }
    if(!data.crypto_agreement) {
      setError('crypto_agreement', {type: 'manual'})
    }
    if(!data.notUs) {
      setError('notUs', {type: 'manual'})
    }
    if(!data.signature) {
      setError('signature', {type: 'manual'})
    }
    if(data.agreement && data.crypto_agreement && data.notUs && data.signature) {
      const fullData = {
        ...data, ...formData
      }
      delete fullData.confirmPassword
      setFetching(true)
      useJwt
        .register(fullData)
        .then(res => {
          setFetching(false)
          if (res.data.error) {
            for (const property in res.data.error) {
              if (res.data.error[property] !== null) {
                setError(property, {
                  type: 'manual',
                  message: res.data.error[property]
                })
              }
            }
          } else {
            toast((t) => (
              <ToastContent
                t={t}
                name={'Success'}
                message='Your account has been successfully registered. You can sign in.'
              />
            ))
            navigate('/login')
          }
        })
        .catch(err => {
          setFetching(false)
          let message
          const errors = err.response.data.errors
          if(errors) {
            message = errors?.[Object.keys(errors)?.[0]]?.details
          } else {
            message = err.response.data.data
          }
          toast((t) => (
            <ToastContent
              t={t}
              name={'Something went wrong'}
              message={ message }
              isError={true}
            />
          ))
        })
    }
  }

  return (
    <Fragment>
      <div className='content-header mb-2'>
        <h2 className='fw-bolder mb-75'>Broker details</h2>
        <span>Enter information about broker.</span>
      </div>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Row className='gx-2 mb-1'>
        <Col md='12' className='mt-1'>
          <div className='form-check form-check-inline'>
          <Controller
              id='agreement'
              name='agreement'
              control={control}
              render={({ field }) => <Input type='checkbox' id='agreement' name='agreement' placeholder='123456' invalid={errors.agreement && true} {...field} />}
            />
              <Label for='agreement' className='form-check-label'>
              I have read, understood, and agree to be bound by <b>Alpaca Securities LLC</b> and <b>Investlink Ltd</b> account terms, and all other terms, disclosures and disclaimers applicable to me, as referenced in the <a className='app-link' target='_blank' href="https://files.alpaca.markets/disclosures/library/AcctAppMarginAndCustAgmt.pdf">Alpaca Customer Agreement</a>. I also acknowledge that the Alpaca Customer Agreement contains a pre-dispute arbitration clause in Section 43.
              </Label>
            </div>
        </Col>
        <Col md='12' className='mt-1'>
          <div className='form-check form-check-inline'>
          <Controller
              id='crypto_agreement'
              name='crypto_agreement'
              control={control}
              render={({ field }) => <Input type='checkbox' id='crypto_agreement' name='crypto_agreement' placeholder='123456' invalid={errors.crypto_agreement && true} {...field} />}
            />
              <Label for='crypto_agreement' className='form-check-label'>
              I have read, understood, and agree to be bound by <b>Alpaca Crypto LLC</b> and <b>Investlink Ltd</b> account terms, and all other terms, disclosures and disclaimers applicable to me, as referenced in the <a className='app-link' target='_blank' href="https://files.alpaca.markets/disclosures/library/Crypto%20Customer%20Agreement.pdf">Alpaca Crypto Customer Agreement</a>. I also acknowledge that the Alpaca Crypto Customer Agreement contains a pre-dispute arbitration clause in Section 26.
              </Label>
            </div>
        </Col>
        <Col md='12' className='mt-1'>
          <div className='form-check form-check-inline'>
          <Controller
              id='notUs'
              name='notUs'
              control={control}
              render={({ field }) => <Input type='checkbox' id='notUs' name='notUs' placeholder='123456' invalid={errors.notUs && true} {...field} />}
            />
              <Label for='notUs' className='form-check-label'>
              I certify that I am not a US citizen, US resident alien or other US person for US tax purposes, and I am submitting the applicable Form W-8 BEN with this form to certify my foreign status and, if applicable, claim tax treaty benefits. 
              </Label>
            </div>
        </Col>
        <Col md='12' className='mt-1'>
          <div className='form-check form-check-inline'>
          <Controller
              id='signature'
              name='signature'
              control={control}
              render={({ field }) => <Input type='checkbox' id='signature' name='signature' placeholder='123456' invalid={errors.signature && true} {...field} />}
            />
              <Label for='signature' className='form-check-label'>
              I understand I am signing this agreement electronically, and that my electronic signature will have the same effect as physically signing and returning the Application Agreement.
              </Label>
            </div>
        </Col>
        <div id={onfidoContainerId}></div>
        </Row>
        <div className='d-flex justify-content-between mt-2'>
          <Button color='secondary' className='btn-prev' outline onClick={() => stepper.previous()}>
            <ChevronLeft size={14} className='align-middle me-sm-25 me-0'></ChevronLeft>
            <span className='align-middle d-sm-inline-block d-none'>Previous</span>
          </Button>
          <Button type='submit' color='success' className='btn-next' disabled={isFetching}>
            <span className='align-middle d-sm-inline-block d-none'>{!isFetching ? 'Submit' : <Spinner size='sm' />}</span>
            <Check size={14} className='align-middle ms-sm-25 ms-0'></Check>
          </Button>
        </div>
      </Form>
    </Fragment>
  )
}

export default BrokerRequirements
