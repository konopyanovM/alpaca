// ** React Imports
import { Fragment, useState } from 'react'

import { useForm, Controller } from 'react-hook-form'
import { ChevronLeft, ChevronRight} from 'react-feather'
// ** Reactstrap Imports
import { Form, Label, Input, Row, Col, Button } from 'reactstrap'

import FileUploaderSingle from '../FileUploaderSingle'

const TaxRequirements = ({ stepper, setFormData }) => {

  // ** States
  const {
    control,
    setError,
    handleSubmit,
    formState: { errors }
  } = useForm({
    defaultValues: {
      is_not_usa: false,
      is_us_tax_form: false,
      w8ben: null
    }
  })

  const [usTax, setUsTax] = useState(null)
  const [usTaxValid, setUsTaxValid] = useState(null)

  const onSubmit = data => {
    if(usTax === null || usTax.length === 0) {
      setUsTaxValid(false)
    } else {
      setUsTaxValid(true)
    }
    if(!data.is_not_usa) {
      setError('is_not_usa', {type: 'manual'})
    }
    if(!data.is_us_tax_form) {
      setError('is_us_tax_form', {type: 'manual'})
    }
    if((usTax === null || usTax.length === 0)) {
      return
    }

    // const usTaxData = new FormData()
    // usTaxData.set('file', usTax[0])
    // data.us_tax_form = usTaxData

    data.us_tax_form = usTax[0]
    
    if(data.is_not_usa && data.is_us_tax_form) {
      setFormData((prev)=>({...prev, ...data}))
      stepper.next()
    }
  }

  return (
    <Fragment>
      <div className='content-header mb-2'>
        <h2 className='fw-bolder mb-75'>Tax details</h2>
        <span>Enter information about your tax details.</span>
      </div>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Row className='gx-2 mb-1'>
          <Col sm='12'>
            <FileUploaderSingle title={'US Tax Form (W8-BEN)'} data={usTax} setData={setUsTax} isValid={usTaxValid} fileTypes={['application/pdf']}/>
            {usTaxValid === null || usTaxValid || <small className='mx-1 text-danger'>Upload the W-8BEN form</small>}
          </Col>
        <Col md='12' className='mt-1'>
          <div className='form-check form-check-inline'>
          <Controller
              id='is_not_usa'
              name='is_not_usa'
              control={control}
              render={({ field }) => <Input type='checkbox' id='is_not_usa' name='is_not_usa' placeholder='123456' invalid={errors.is_not_usa && true} {...field} />}
            />
              <Label for='is_not_usa' className='form-check-label'>
              I certify that I am not a US citizen, US resident alien or other US person for US tax purposes, and I am submitting the applicable Form W-8 BEN with this form to certify my foreign status and, if applicable, claim tax treaty benefits. 
              </Label>
            </div>
        </Col>
        <Col md='12' className='mt-1'>
          <div className='form-check form-check-inline'>
          <Controller
              id='is_us_tax_form'
              name='is_us_tax_form'
              control={control}
              render={({ field }) => <Input type='checkbox' id='is_us_tax_form' name='is_us_tax_form' placeholder='123456' invalid={errors.is_us_tax_form && true} {...field} />}
            />
              <Label for='is_us_tax_form' className='form-check-label'>
              Under penalties of perjury, I declare that I have examined the information on this form and to the best of my knowledge and belief it is true, correct, and complete. I further certify under penalties of perjury that:
              <ul>
                <li>I am the individual that is the beneficial owner (or am authorized to sign for the individual that is the beneficial owner) of all the income or
proceeds to which this form relates or am using this form to document myself for chapter 4 purposes;</li>
                <li>The person named on line 1 of this form is not a U.S. person;</li>
                <li>
                This form relates to:
                  <ol style={{listStyleType: 'lower-alpha'}}>
                    <li>income not effectively connected with the conduct of a trade or business in the United States;</li>
                    <li>income effectively connected with the conduct of a trade or business in the United States but is not subject to tax under an applicable</li>
                    <li>the partner’s share of a partnership’s effectively connected taxable income; or</li>
                    <li>the partner’s amount realized from the transfer of a partnership interest subject to withholding under section 1446(f);</li>
                  </ol>
                </li>
                <li>The person named on line 1 of this form is a resident of the treaty country listed on line 9 of the form (if any) within the meaning of the
income tax treaty between the United States and that country; and</li>
                <li>For broker transactions or barter exchanges, the beneficial owner is an exempt foreign person as defined in the instructions.
Furthermore, I authorize this form to be provided to any withholding agent that has control, receipt, or custody of the income of which I am the beneficial owner or any withholding agent that can disburse or make payments of the income of which I am the beneficial owner. I agree that I will submit a new form within 30 days if any certification made on this form becomes incorrect.</li>
              </ul>
              </Label>
            </div>
        </Col>
        </Row>
        <div className='d-flex justify-content-between mt-2'>
          <Button color='secondary' className='btn-prev' outline onClick={() => stepper.previous()}>
            <ChevronLeft size={14} className='align-middle me-sm-25 me-0'></ChevronLeft>
            <span className='align-middle d-sm-inline-block d-none'>Previous</span>
          </Button>
          <Button type='submit' color='primary' className='btn-next'>
            <span className='align-middle d-sm-inline-block d-none'>Next</span>
            <ChevronRight size={14} className='align-middle ms-sm-25 ms-0'></ChevronRight>
          </Button>
        </div>
      </Form>
    </Fragment>
  )
}

export default TaxRequirements
