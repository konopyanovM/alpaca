// ** Third Party Components
import Select from 'react-select'

// ** React Imports
import { Fragment, useState, useEffect } from 'react'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
// ** Third Party Components
import { useForm, Controller } from 'react-hook-form'
import { ChevronLeft, ChevronRight } from 'react-feather'

// ** Reactstrap Imports
import { Form, Label, Input, Row, Col, Button, FormFeedback, Alert } from 'reactstrap'
import "cleave.js/dist/addons/cleave-phone.kz";

// ** Utils
import { selectThemeColors } from '@utils'
import Popover from '../Popover'
import FileUploaderSingle from '../FileUploaderSingle'

const defaultValues = {
  annual_income_min: 0,
  annual_income_max: 0,
  liquid_net_worth_min: '0',
  liquid_net_worth_max: '20000',
  funding_source: 'Employment income',
  employment_status: 'Employed',
  employer_name: '',
  employer_address: '',
  employment_position: '',
  is_affiliated_exchange_or_finra: false,
  is_control_person: false,
  is_politically_exposed: false,
  immediate_family_exposed: false,

}

const incomeOptions = [
  { value: null, label: 'Choose an option' },
  { value: [0, 20000], label: '0 - $20,000' },
  { value: [20000, 49999], label: '$20,000 - $49,999' },
  { value: [50000, 99999], label: '$50,000 - $99,999' },
  { value: [100000, 499999], label: '$100,000 - $499,999' },
  { value: [500000, 999999], label: '$500,000 - $999,999' },
  { value: [1000000, 9999999], label: '$1,000,000 +' }
]

const liquidAssets = [
  { value: [0, 20000], label: '0 - $20,000' },
  { value: [20000, 49999], label: '$20,000 - $49,999' },
  { value: [50000, 99999], label: '$50,000 - $99,999' },
  { value: [100000, 499999], label: '$100,000 - $499,999' },
  { value: [500000, 999999], label: '$500,000 - $999,999' },
  { value: [1000000, 9999999], label: '$1,000,000 +' }
]

const fundingSource = [
  { value: 'employment_income', label: 'Employment income' },
  { value: 'investments', label: 'Investments' },
  { value: 'inheritance', label: 'Inheritance' },
  { value: 'business_income', label: 'Business income' },
  { value: 'savings', label: 'Savings' },
  { value: 'family ', label: 'Family' }
]

const employmentStatus = [
  { value: 'employed', label: 'Employed' },
  { value: 'unemployed', label: 'Unemployed' },
  { value: 'student', label: 'Student' },
  { value: 'retired ', label: 'Retired' }
]

const FinancialProfileAndAffiliations = ({ stepper, setFormData }) => {
  const Schema = yup.object().shape({
    annual_income_min: yup.number(),
    annual_income_max: yup.number(),
    liquid_net_worth_min: yup.string().required(),
    liquid_net_worth_max: yup.string().required(),
    funding_source: yup.string().required(),
    employment_status: yup.string().required(),
    employer_name: yup.string(),
    employer_address: yup.string(),
    employment_position: yup.string(),
  })
  // ** Hooks
  const {
    control,
    handleSubmit,
    watch,
    setValue,
    formState: { errors }
  } = useForm({
    defaultValues,
    resolver: yupResolver(Schema)
  })
  const [annual, setAnnual] = useState(incomeOptions[0])
  const [liquid, setLiquid] = useState(liquidAssets[0])
  const [funding, setFunding] = useState(fundingSource[0])
  const [employment, setEmployment] = useState(employmentStatus[0])

  const [affiliationFile, setAffiliationFile] = useState(null)
  const [companyFile, setCompanyFile] = useState(null)

  const [affiliationFileValid, setAffiliationFileValid] = useState(null)
  const [companyFileValid, setCompanyFileValid] = useState(null)

  const onSubmit = data => {
    if (annual.value) {
      data.annual_income_min = annual.value[0]
      data.annual_income_max = annual.value[1]
    }
    data.liquid_net_worth_min = liquid.value[0]
    data.liquid_net_worth_max = liquid.value[1]
    data.funding_source = funding.value
    data.employment_status = employment.value

    if ((watch('is_affiliated_exchange_or_finra')) && (affiliationFile === null || affiliationFile.length === 0)) {
      setAffiliationFileValid(false)
    } else {
      setAffiliationFileValid(true)
    }
    if ((watch('is_control_person') && (companyFile === null || companyFile.length === 0))) {
      setCompanyFileValid(false)
    } else {
      setCompanyFileValid(true)
    }
    if (((affiliationFile === null || affiliationFile.length === 0) || (companyFile === null || companyFile.length === 0)) && (watch('is_affiliated_exchange_or_finra') || watch('is_control_person'))) {
      return
    }

    if (affiliationFile) {
      //   const affiliationData = new FormData()
      //   affiliationData.set('image', affiliationFile[0])
      //   data.affiliations_document = affiliationData
      data.affiliations_document = affiliationFile[0]
    }

    if (companyFile) {
      //   const companyData = new FormData()
      //   companyData.set('image', companyFile[0])
      //   data.public_company = companyData
      data.public_company = companyFile[0]
    }

    setFormData((prev) => ({ ...prev, ...data }))
    stepper.next()
  }

  useEffect(() => {
    setValue('immediate_family_exposed', watch('is_politically_exposed'))
  }, [watch('is_politically_exposed')])

  return (
    <Fragment>
      <div className='content-header mb-2'>
        <h2 className='fw-bolder mb-75'>Financial profile and Affiliations</h2>
        <span>Enter financial information and check affiliations.</span>
      </div>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Row>
          <Col className='mb-1' md='6' sm='12'>
            <span className='d-flex justify-content-between align-items-center'>
              <Label className='form-label'>Annual household income <span className='text-secondary'>(optional)</span></Label>
              <Popover target={'annual'} text={'Annual household income includes income from sources such as employment, alimony, social security, investment income, etc.'}></Popover>
            </span>
            <Select
              theme={selectThemeColors}
              className='react-select'
              classNamePrefix='select'
              value={annual}
              options={incomeOptions}
              isClearable={false}
              onChange={(data) => { setAnnual(data) }}
            />
          </Col>
          <Col className='mb-1' md='6' sm='12'>
            <span className='d-flex justify-content-between align-items-center'>
              <Label className='form-label'>Investible / Liquid assets</Label>
              <Popover target={'liquid'} text={'Investible / Liquid assets is your net worth minus assets that cannot be converted quickly and easily into cash, such as real estate, business equity, personal property and automobiles, expected inheritances, assets earmarked for other purposes, and investments or accounts subject to substantial penalties if they were sold or if assets were withdrawn from them.'}></Popover>
            </span>
            <Select
              theme={selectThemeColors}
              className='react-select'
              classNamePrefix='select'
              value={liquid}
              options={liquidAssets}
              isClearable={false}
              onChange={(data) => { setLiquid(data) }}
            />
            {errors.liquid_net_worth_min && <small className='mx-1 text-danger opacity-75'>{errors.liquid_net_worth_min.message}</small>}
          </Col>
        </Row>
        <Row>
          <Col className='mb-1' md='6' sm='12'>
            <Label className='form-label'>Account funding source</Label>
            <Select
              theme={selectThemeColors}
              className='react-select'
              classNamePrefix='select'
              value={funding}
              options={fundingSource}
              isClearable={false}
              onChange={(data) => { setFunding(data) }}
            />
          </Col>
          <Col className='mb-1' md='6' sm='12'>
            <Label className='form-label'>Employment Status</Label>
            <Select
              theme={selectThemeColors}
              className='react-select'
              classNamePrefix='select'
              value={employment}
              options={employmentStatus}
              isClearable={false}
              onChange={(data) => { setEmployment(data) }}
            />
          </Col>
        </Row>
        <Row>
          <Col md='4' className='mb-1'>
            <Label className='form-label' for='employer_name'>
              Name of Employer <span className='text-secondary'>(optional)</span>
            </Label>
            <Controller
              id='employer_name'
              name='employer_name'
              control={control}
              render={({ field }) => <Input name='employer_name' invalid={errors.employer_name && true} {...field} />}
            />
            {errors.employer_name && <FormFeedback>{errors.employer_name.message}</FormFeedback>}
          </Col>
          <Col md='4' className='mb-1'>
            <Label className='form-label' for='employer_address'>
              Employer address <span className='text-secondary'>(optional)</span>
            </Label>
            <Controller
              id='employer_address'
              name='employer_address'
              control={control}
              render={({ field }) => <Input invalid={errors.employer_address && true} {...field} />}
            />
            {errors.employer_address && <FormFeedback>{errors.employer_address.message}</FormFeedback>}
          </Col>
          <Col md='4' className='mb-1'>
            <Label className='form-label' for='employment_position'>
              Occupation / Job Title <span className='text-secondary'>(optional)</span>
            </Label>
            <Controller
              id='employment_position'
              name='employment_position'
              control={control}
              render={({ field }) => <Input invalid={errors.employment_position && true} {...field} />}
            />
            {errors.employment_position && <FormFeedback>{errors.employment_position.message}</FormFeedback>}
          </Col>
        </Row>
        <Alert color='info'>
          <div className='alert-body'>
            You can skip filling out these forms now, but you can fill them out later.
          </div>
        </Alert>
        <hr />

        <Row>
          <Col md='12' className='mt-1'>
            <div className='form-check form-check-inline'>
              <Controller
                id='is_affiliated_exchange_or_finra'
                name='is_affiliated_exchange_or_finra'
                control={control}
                render={({ field }) => <Input type='checkbox' id='is_affiliated_exchange_or_finra' name='is_affiliated_exchange_or_finra' placeholder='123456' invalid={errors.is_affiliated_exchange_or_finra && true} {...field} />}
              />
              <Label for='is_affiliated_exchange_or_finra' className='form-check-label'>
                Are you or an immediate family member affiliated with or employed by a stock exchange, regulatory body, member firm of an exchange, FINRA or a municipal securities broker-dealer?
              </Label>
            </div>
          </Col>
          {watch('is_affiliated_exchange_or_finra') ?
            <Col sm='12'>
              <FileUploaderSingle title={'Upload file'} data={affiliationFile} setData={setAffiliationFile} isValid={affiliationFileValid} fileTypes={['application/pdf']} />
              {affiliationFileValid === null || affiliationFileValid || <small className='mx-1 text-danger'>Upload the file</small>}
            </Col>
            : null
          }
        </Row>
        <Row>
          <Col md='12' className='mt-1'>
            <div className='form-check form-check-inline'>
              <Controller
                id='is_control_person'
                name='is_control_person'
                control={control}
                render={({ field }) => <Input type='checkbox' id='is_control_person' name='is_control_person' placeholder='123456' invalid={errors.is_control_person && true} {...field} />}
              />
              <Label for='is_control_person' className='form-check-label'>
                Are you or an immediate family member an officer or 10% or greater shareholder of a publicly traded company, subject to the US Securities Exchange Act 1934?
              </Label>
            </div>
          </Col>
          {watch('is_control_person') ?
            <Col sm='12'>
              <FileUploaderSingle title={'Upload file'} data={companyFile} setData={setCompanyFile} isValid={companyFileValid} fileTypes={['application/pdf']} />
              {companyFileValid === null || companyFileValid || <small className='mx-1 text-danger'>Upload the file</small>}
            </Col>
            : null
          }
        </Row>
        <Row>
          <Col md='12' className='mt-1'>
            <div className='form-check form-check-inline'>
              <Controller
                id='is_politically_exposed'
                name='is_politically_exposed'
                control={control}
                render={({ field }) => <Input type='checkbox' id='is_politically_exposed' name='is_politically_exposed' placeholder='123456' invalid={errors.is_politically_exposed && true} {...field} />}
              />
              <Label for='is_politically_exposed' className='form-check-label'>
                Are you or an immediate family member currently or formerly a Politically Exposed Person or Public Official?
              </Label>
            </div>
          </Col>
        </Row>
        <div className='d-flex justify-content-between mt-2'>
          <Button color='secondary' className='btn-prev' outline onClick={() => stepper.previous()}>
            <ChevronLeft size={14} className='align-middle me-sm-25 me-0'></ChevronLeft>
            <span className='align-middle d-sm-inline-block d-none'>Previous</span>
          </Button>
          <Button type='submit' color='primary' className='btn-next'>
            <span className='align-middle d-sm-inline-block d-none'>Next</span>
            <ChevronRight size={14} className='align-middle ms-sm-25 ms-0'></ChevronRight>
          </Button>
        </div>
      </Form>
    </Fragment>
  )
}

export default FinancialProfileAndAffiliations
