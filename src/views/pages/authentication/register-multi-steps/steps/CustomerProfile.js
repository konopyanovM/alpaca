// ** React Imports
import { Fragment, useState } from 'react'

// ** Third Party Components
import * as yup from 'yup'
import { useForm, Controller } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { ChevronLeft, ChevronRight } from 'react-feather'

// ** Reactstrap Imports
import { Form, Label, Input, Row, Col, Button, FormFeedback } from 'reactstrap'

// ** Custom Components
import InputPasswordToggle from '@components/input-password-toggle'
import classNames from 'classnames'
import Cleave from 'cleave.js/react'
import Select from 'react-select'
import { selectThemeColors } from '@utils'

import countryOptions from './../countryOptions'
import '@styles/react/libs/react-select/_react-select.scss'

const defaultValues = {
  email: '',
  password: '',
  confirm_password: '',
  first_name: '',
  middle_name: '',
  last_name: '',
  phone_number: '',
  postal_code: '',
  street_address: '',
  date_of_birth: '',
  state: '',
  city: '',
  tax_id: '',
  tax_id_type: '',
  unit_apt: ''
}

const taxIdTypeOptions = [
  { value: 'USA_SSN', label: 'USA SSN'},
  { value: 'ARG_AR_CUIT', label: 'ARG AR CUIT'},
  { value: 'AUS_TFN', label: 'AUS TFN'},
  { value: 'AUS_ABN', label: 'AUS ABN'},
  { value: 'BOL_NIT', label: 'BOL NIT'},
  { value: 'BRA_CPF', label: 'BRA CPF'},
  { value: 'CHL_RUT', label: 'CHL RUT'},
  { value: 'COL_NIT', label: 'COL NIT'},
  { value: 'CRI_NITE', label: 'CRI NITE'},
  { value: 'DEU_TAX_ID', label: 'DEU TAX ID'},
  { value: 'DOM_RNC', label: 'DOM RNC'},
  { value: 'ECU_RUC', label: 'ECU RUC'},
  { value: 'FRA_SPI', label: 'FRA SPI'},
  { value: 'GBR_UTR', label: 'GBR UTR'},
  { value: 'GBR_NINO', label: 'GBR NINO'},
  { value: 'GTM_NIT', label: 'GTM NIT'},
  { value: 'HND_RTN', label: 'HND RTN'},
  { value: 'IDN_KTP', label: 'IDN_KTP'},
  { value: 'IND_PAN', label: 'IND PAN'},
  { value: 'ISR_TAX_ID', label: 'ISR TAX ID'},
  { value: 'ITA_TAX_ID', label: 'ITA TAX ID'},
  { value: 'JPN_TAX_ID', label: 'JPN TAX ID'},
  { value: 'MEX_RFC', label: 'MEX RFC'},
  { value: 'NIC_RUC', label: 'NIC RUC'},
  { value: 'NLD_TIN', label: 'NLD TIN'},
  { value: 'PAN_RUC', label: 'PAN RUC'},
  { value: 'PER_RUC', label: 'PER RUC'},
  { value: 'PRY_RUC', label: 'PRY RUC'},
  { value: 'SGP_NRIC', label: 'SGP NRIC'},
  { value: 'SGP_FIN', label: 'SGP FIN'},
  { value: 'SGP_ASGD', label: 'SGP ASGD'},
  { value: 'SGP_ITR', label: 'SGP ITR'},
  { value: 'SLV_NIT', label: 'SLV NIT'},
  { value: 'SWE_TAX_ID', label: 'SWE TAX ID'},
  { value: 'URY_RUT', label: 'URY RUT'},
  { value: 'VEN_RIF', label: 'VEN RIF'},
  { value: 'NOT_SPECIFIED', label: 'NOT SPECIFIED'},
]

const CustomerProfile = ({ stepper, setFormData }) => {
  const Schema = yup.object().shape({
    email: yup.string().email().required('Email is required field'),
    password: yup.string().required('Password is required field'),
    confirm_password: yup
      .string()
      .required()
      .oneOf([yup.ref(`password`), null], 'Passwords must match'),
    first_name: yup.string().required('First name is required field'),
    middle_name: yup.string(),
    last_name: yup.string().required('Last name is required field'),
    phone_number: yup.string().required('Phone number is required field'),
    postal_code: yup.string(),
    street_address: yup.string().required('Street address is required field'),
    date_of_birth: yup.string().required('Date of birth is required field'),
    state: yup.string(),
    city: yup.string().required('City is required field'),
    tax_id: yup.string().matches('([0-9-])+', 'Incorrect tax id'),
    unit_apt: yup.string(),
  })

  // ** Hooks

  const {
    control,
    handleSubmit,
    setError,
    formState: { errors }
  } = useForm({
    defaultValues,
    resolver: yupResolver(Schema)
  })

  const [taxIdType, setTaxIdType] = useState(taxIdTypeOptions[36])

  const [country, setCountry] = useState(countryOptions[114])
  const [citizenship, setCitizenship] = useState(countryOptions[114])
  const [taxResidence, setTaxResidence] = useState(countryOptions[114])

  const onSubmit = data => {
    const date = new Date(data.date_of_birth)
    const validDate = new Date()
    validDate.setFullYear(validDate.getFullYear() - 18)

    if(date > validDate) {
      setError('date_of_birth', {
        type: 'manual',
        message: 'You must be over 18 years of age'
      })
      return
    }
      data.tax_id_type = taxIdType.value
      data.country = country.value
      data.country_of_citizenship = citizenship.value
      data.country_of_tax_residence = taxResidence.value
      setFormData((prev)=>({...prev, ...data}))
      stepper.next()
  }

  return (
    <Fragment>
      <div className='content-header mb-2'>
        <h2 className='fw-bolder mb-75'>Customer profile</h2>
        <span>Enter your email and password details</span>
      </div>
      <Form onSubmit={handleSubmit(onSubmit)}>
      <Row>
          <Col sm={12} className='mb-1'>
            <Label className='form-label' for='email'>
              Email
            </Label>
            <Controller
              control={control}
              id='email'
              name='email'
              render={({ field }) => (
                <Input type='email' placeholder='john.doe@email.com' invalid={errors.email && true} {...field} />
              )}
            />
            {errors.email && <FormFeedback>{errors.email.message}</FormFeedback>}
          </Col>
      </Row>
      <Row>
        <div className='form-password-toggle col-md-6 mb-1'>
          <Controller
            id='password'
            name='password'
            control={control}
            render={({ field }) => (
              <InputPasswordToggle
                label='Password'
                htmlFor='password'
                className='input-group-merge'
                invalid={errors.password && true}
                {...field}
              />
            )}
          />
          {errors.password && <FormFeedback>{errors.password.message}</FormFeedback>}
        </div>
        <div className='form-password-toggle col-md-6 mb-1'>
          <Controller
            control={control}
            id='confirm_password'
            name='confirm_password'
            render={({ field }) => (
              <InputPasswordToggle
                label='Confirm Password'
                htmlFor='confirm_password'
                className='input-group-merge'
                invalid={errors.confirm_password && true}
                {...field}
              />
            )}
          />
          {errors.confirm_password && <FormFeedback>{errors.confirm_password.message}</FormFeedback>}
        </div>
      </Row>
      <Row>
      <Col sm={6} className='mb-1'>
          <Label className='form-label' for='first_name'>
            First name
          </Label>
          <Controller
            control={control}
            id='first_name'
            name='first_name'
            render={({ field }) => (
              <Input type='first_name' placeholder='John' invalid={errors.first_name && true} {...field} />
            )}
          />
          {errors.first_name && <FormFeedback>{errors.first_name.message}</FormFeedback>}
        </Col>
        <Col sm={6} className='mb-1'>
          <Label className='form-label' for='last_name'>
            Last name
          </Label>
          <Controller
            control={control}
            id='last_name'
            name='last_name'
            render={({ field }) => (
              <Input type='last_name' placeholder='Doe' invalid={errors.last_name && true} {...field} />
            )}
          />
          {errors.last_name && <FormFeedback>{errors.last_name.message}</FormFeedback>}
        </Col>
      </Row>
      <Row>
        <Col md='6' className='mb-1'>
            <Label className='form-label' for='phone_number'>
              Phone number
            </Label>
            <Controller
              name='phone_number'
              control={control}
              render={({ field }) => (
              <Cleave
                {...field}
                placeholder='8 777 123 4567'
                className={classNames('form-control', { 'is-invalid': errors.phone_number })}
                options={{
                  phone: true,
                  phoneRegionCode: '{country}'
                }}
              />)}
            />
            {errors.phone_number && <FormFeedback>{errors.phone_number.message}</FormFeedback>}
          </Col>
          <Col md='6' className='mb-1'>
            <Label className='form-label' for='postal_code'>
              Postal code <span className='text-secondary'>(optional)</span>
            </Label>
            <Controller
              id='postal_code'
              name='postal_code'
              control={control}
              render={({ field }) => <Input type='number' name='postal_code' placeholder='123456' invalid={errors.postal_code && true} {...field} />}
            />
            {errors.postal_code && <FormFeedback>{errors.postal_code.message}</FormFeedback>}
          </Col>
      </Row>
      <Row>
      <Col md='6' className='mb-1'>
          <Label className='form-label' for='date_of_birth'>
            Date of birth
          </Label>
          <Controller
            id='date_of_birth'
            name='date_of_birth'
            control={control}
            render={({ field }) => <Input type='date' name='date_of_birth' invalid={errors.date_of_birth && true} {...field} />}
          />
          {errors.date_of_birth && <FormFeedback>{errors.date_of_birth.message}</FormFeedback>}
        </Col>
        <Col sm='6' className='mb-1'>
          <Label className='form-label' for='street_address'>
            Street address
          </Label>
          <Controller
            id='street_address'
            name='street_address'
            control={control}
            render={({ field }) => <Input placeholder='Wall street' invalid={errors.street_address && true} {...field} />}
          />
          {errors.street_address && <FormFeedback>{errors.street_address.message}</FormFeedback>}
        </Col>
      </Row>
      <Row>
        <Col md='4' className='mb-1'>
          <Label className='form-label' for='tax_id'>
            Tax id
          </Label>
          <Controller
            id='tax_id'
            name='tax_id'
            control={control}
            render={({ field }) => <Input placeholder='123-123' invalid={errors.tax_id && true} {...field} />}
          />
          {errors.tax_id && <FormFeedback>{errors.tax_id.message}</FormFeedback>}
        </Col>
        <Col className='mb-1' md='4'>
            <Label className='form-label'>Tax id type</Label>
            <Select
              theme={selectThemeColors}
              className='react-select'
              classNamePrefix='select'
              value={taxIdType}
              options={taxIdTypeOptions}
              isClearable={false}
              onChange={(data) => { setTaxIdType(data) }}
              />
              {errors.tax_id_type && <FormFeedback>{errors.tax_id_type.message}</FormFeedback>}
          </Col>
        <Col md='4' className='mb-1'>
          <Label className='form-label' for='unit_apt'>
            Unit / Apt # <span className='text-secondary'>(optional)</span>
          </Label>
          <Controller
            id='unit_apt'
            name='unit_apt'
            control={control}
            render={({ field }) => <Input placeholder='' invalid={errors.unit_apt && true} {...field} />}
          />
          {errors.unit_apt && <FormFeedback>{errors.unit_apt.message}</FormFeedback>}
        </Col>
      </Row>
      <Row>
        <Col className='mb-1' md='4'>
            <Label className='form-label'>Сountry</Label>
            <Select
              theme={selectThemeColors}
              className='react-select'
              classNamePrefix='select'
              value={country}
              options={countryOptions}
              isClearable={false}
              onChange={(data) => { setCountry(data) }}
              />
              {errors.country && <FormFeedback>{errors.country.message}</FormFeedback>}
        </Col>
        <Col md='4' className='mb-1'>
              <Label className='form-label' for='state'>
                State <span className='text-secondary'>(optional)</span>
              </Label>
              <Controller
                id='state'
                name='state'
                control={control}
                render={({ field }) => <Input placeholder='Almaty' invalid={errors.state && true} {...field} />}
              />
              {errors.state && <FormFeedback>{errors.state.message}</FormFeedback>}
        </Col>
        <Col md='4' className='mb-1'>
          <Label className='form-label' for='city'>
            City
          </Label>
          <Controller
            id='city'
            name='city'
            control={control}
            render={({ field }) => <Input placeholder='Almaty' invalid={errors.city && true} {...field} />}
          />
          {errors.city && <FormFeedback>{errors.city.message}</FormFeedback>}
        </Col>
      </Row>
      <Row>
        <Col className='mb-1' md='6'>
            <Label className='form-label'>Сountry of citizenship</Label>
            <Select
              theme={selectThemeColors}
              className='react-select'
              classNamePrefix='select'
              value={citizenship}
              options={countryOptions}
              isClearable={false}
              onChange={(data) => { setCitizenship(data) }}
              />
              {errors.country_of_citizenship && <FormFeedback>{errors.country_of_citizenship.message}</FormFeedback>}
        </Col>
        <Col className='mb-1' md='6'>
            <Label className='form-label'>Сountry of tax residence</Label>
            <Select
              theme={selectThemeColors}
              className='react-select'
              classNamePrefix='select'
              value={taxResidence}
              options={countryOptions}
              isClearable={false}
              onChange={(data) => { setTaxResidence(data) }}
              />
              {errors.country_of_tax_residence && <FormFeedback>{errors.country_of_tax_residence.message}</FormFeedback>}
        </Col>
      </Row>
        <div className='d-flex justify-content-between mt-2'>
          <Button color='secondary' className='btn-prev' outline disabled>
            <ChevronLeft size={14} className='align-middle me-sm-25 me-0'></ChevronLeft>
            <span className='align-middle d-sm-inline-block d-none'>Previous</span>
          </Button>
          <Button type='submit' color='primary' className='btn-next'>
            <span className='align-middle d-sm-inline-block d-none'>Next</span>
            <ChevronRight size={14} className='align-middle ms-sm-25 ms-0'></ChevronRight>
          </Button>
        </div>
      </Form>
    </Fragment>
  )
}

export default CustomerProfile
