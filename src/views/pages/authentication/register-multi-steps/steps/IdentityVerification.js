// ** React Imports
import { Fragment, useState } from 'react'

import { useForm } from 'react-hook-form'
import { ChevronLeft, ChevronRight } from 'react-feather'
// ** Reactstrap Imports
import { Form, Row, Button, Col } from 'reactstrap'
import FileUploaderSingle from '../FileUploaderSingle'
import '@styles/react/libs/file-uploader/file-uploader.scss'

const IdentityVerifications = ({ stepper, setFormData }) => {
  const [idFront, setIdFront] = useState(null)
  const [idBack, setIdBack] = useState(null)
  const [proofOfAddress, setProofOfAddress] = useState(null)

  const [idFrontValid, setIdFrontValid] = useState(null)
  const [idBackValid, setIdBackValid] = useState(null)
  const [proofOfAddressValid, setProofOfAddressValid] = useState(null)

  const {
    handleSubmit,
  } = useForm()

  const onSubmit = () => {
    const data = {
      id_front: null,
      id_back: null,
      proof_of_address: null,
    }

    if(idFront === null || idFront.length === 0) {
      setIdFrontValid(false)
    } else {
      setIdFrontValid(true)
    }
    if(idBack === null || idBack.length === 0) {
      setIdBackValid(false)
    } else {
      setIdBackValid(true)
    }
    if(proofOfAddress === null || proofOfAddress.length === 0) {
      setProofOfAddressValid(false)
    } else {
      setProofOfAddressValid(true)
    }
    if((idFront === null || idFront.length === 0) || (idBack === null || idBack.length === 0) || (proofOfAddress === null || proofOfAddress.length === 0)) {
      return
    }

    // const idFrontData = new FormData()
    // const idBackData = new FormData()
    // const proofOfAddressData = new FormData()

    // idFrontData.set('image', idFront[0])
    // idBackData.set('image', idBack[0])
    // proofOfAddressData.set('file', proofOfAddress[0])

    // data.photo_id_front = idFrontData
    // data.photo_id_back = idBackData
    // data.proof_of_address = proofOfAddressData
    data.photo_id_front = idFront[0]
    data.photo_id_back = idBack[0]
    data.proof_of_address = proofOfAddress[0]

    setFormData((prev)=>({...prev, ...data}))
    stepper.next()
  }

  return (
    <Fragment>
      <div className='content-header mb-2'>
        <h2 className='fw-bolder mb-75'>Identity verification</h2>
        <span>Verify your identity.</span>
      </div>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Row className='gx-2 mb-1'>
          <Col sm='4'>
            <FileUploaderSingle title={'ID - front'} data={idFront} setData={setIdFront} isValid={idFrontValid} fileTypes={['image/png', 'image/jpeg']}/>
            {idFrontValid === null || idFrontValid || <small className='mx-1 text-danger'>Upload the front of your id</small>}
          </Col>
          <Col sm='4'>
            <FileUploaderSingle title={'ID - back'} data={idBack} setData={setIdBack} isValid={idBackValid} fileTypes={['image/png', 'image/jpeg']}/>
            {idBackValid === null ||  idBackValid || <small className='mx-1 text-danger'>Upload the back of your id</small>}
          </Col>
          <Col sm='4'>
            <FileUploaderSingle title={'Proof of address'} data={proofOfAddress} setData={setProofOfAddress} isValid={proofOfAddressValid} fileTypes={['application/pdf']}/>
            {proofOfAddressValid === null ||  proofOfAddressValid || <small className='mx-1 text-danger'>Upload the proof of address</small>}
          </Col>
        </Row>
        <div className='d-flex justify-content-between mt-2'>
          <Button color='secondary' className='btn-prev' outline onClick={() => stepper.previous()}>
            <ChevronLeft size={14} className='align-middle me-sm-25 me-0'></ChevronLeft>
            <span className='align-middle d-sm-inline-block d-none'>Previous</span>
          </Button>
          <Button type='submit' color='primary' className='btn-next'>
            <span className='align-middle d-sm-inline-block d-none'>Next</span>
            <ChevronRight size={14} className='align-middle ms-sm-25 ms-0'></ChevronRight>
          </Button>
        </div>
      </Form>
    </Fragment>
  )
}

export default IdentityVerifications
