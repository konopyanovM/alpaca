// ** React Imports
import { Fragment, useEffect } from 'react'

import { useForm } from 'react-hook-form'
import { ChevronLeft, ChevronRight } from 'react-feather'
// ** Reactstrap Imports
import { Form, Row, Button } from 'reactstrap'

import * as OnfidoSDK from "onfido-sdk-ui/dist/onfido.min.js";
import "onfido-sdk-ui/dist/style.css";

const onfidoToken = "onfido-jwt";
const onfidoContainerId = "onfido-sdk-wrapper";

const Onfido = ({ stepper, setFormData, formData }) => {
  // ** States
  const {
    control,
    setError,
    handleSubmit,
    formState: { errors }
  } = useForm({
    defaultValues: {
      agreement: '',
      signature: '',
    }
  })


  useEffect(() => {
    if (onfidoToken)
      OnfidoSDK.init(
        {
        token: onfidoToken,
        containerId: onfidoContainerId
      }
      );
  }, []);

  const onSubmit = data => {

    // setFormData((prev)=>({...prev, ...data}))
    stepper.next()
  }

  return (
    <Fragment>
      <div className='content-header mb-2'>
        <h2 className='fw-bolder mb-75'>Onfido verification</h2>
        <span>Initial buildout.</span>
      </div>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Row className='gx-2 mb-1'>
          <div id={onfidoContainerId}></div>
        </Row>
        <div className='d-flex justify-content-between mt-2'>
          <Button color='secondary' className='btn-prev' outline onClick={() => stepper.previous()}>
            <ChevronLeft size={14} className='align-middle me-sm-25 me-0'></ChevronLeft>
            <span className='align-middle d-sm-inline-block d-none'>Previous</span>
          </Button>
          <Button type='submit' color='primary' className='btn-next'>
            <span className='align-middle d-sm-inline-block d-none'>Next</span>
            <ChevronRight size={14} className='align-middle ms-sm-25 ms-0'></ChevronRight>
          </Button>
        </div>
      </Form>
    </Fragment>
  )
}

export default Onfido
