// ** React Imports
import { useState, useEffect } from 'react'

// ** Styles
import '@styles/react/libs/charts/apex-charts.scss'
import '@styles/base/pages/dashboard-ecommerce.scss'
import OrdersTable from './Table'
import { getDocuments } from '../../../../utility/Services'

const Orders = () => {
  const [data, setData] = useState(null)

  const getData = async () => {
    const { data } = await getDocuments()
    const filteredData = data.documents.filter(item => item.type === 'trade_confirmation_json')
    setData(filteredData)
  }

  useEffect(() => {
    getData()
  }, [])

  return (
    <OrdersTable data={{ data }}></OrdersTable>
  )
}

export default Orders
