// ** User List Component
import OpenTable from './open'
import DownloadTable from './download/Table'

// ** Styles
import '@styles/react/apps/app-users.scss'
import { Col, TabContent, TabPane } from 'reactstrap'

// ** Demo Components
import Tabs from './Tabs'
import { useState } from 'react'

const UsersList = () => {
  // ** States
  const [activeTab, setActiveTab] = useState('1')
  const [data, setData] = useState(null)

  const toggleTab = tab => {
    setActiveTab(tab)
  }

  return (
    <Col xs={12}>
      <Tabs className='mb-2' activeTab={activeTab} toggleTab={toggleTab} />

      <TabContent activeTab={activeTab}>
        <TabPane tabId='1'>
          <OpenTable></OpenTable>
        </TabPane>
        <TabPane tabId='2'>
          <DownloadTable />
        </TabPane>
      </TabContent>
    </Col>
  )
}

export default UsersList
