// ** React Imports
import { Fragment } from 'react'
import { HelpCircle } from 'react-feather'

// ** Reactstrap Imports
import { Button, UncontrolledPopover, PopoverHeader, PopoverBody } from 'reactstrap'

const Popover = ({title = "Tip", text, target}) => {
  return (
    <Fragment>
      <Button.Ripple type='button' className='btn-icon rounded-circle' outline color='flat-primary' id={target} size='sm'>
        <HelpCircle></HelpCircle>
      </Button.Ripple>
      <UncontrolledPopover trigger='focus' placement='top' target={target}>
        <PopoverHeader>{title}</PopoverHeader>
        <PopoverBody>
          {text}
        </PopoverBody>
      </UncontrolledPopover>
    </Fragment>
  )
}
export default Popover
