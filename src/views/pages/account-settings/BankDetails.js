// ** React Imports
import { Fragment, useState, useEffect } from 'react'

// ** Third Party Components
import { useForm, Controller } from 'react-hook-form'
import 'cleave.js/dist/addons/cleave-phone.us'

// ** Reactstrap Imports
import {
  Row,
  Col,
  Form,
  Card,
  Input,
  Label,
  Button,
  CardBody,
  CardTitle,
  CardHeader,
  FormFeedback,
  InputGroup,
  InputGroupText
} from 'reactstrap'


// ** Demo Components
import { createACHRel, getAchRelationship } from '../../../utility/Services'
import { X } from 'react-feather'
import toast from 'react-hot-toast'
import Cleave from 'cleave.js/react'
import classNames from 'classnames'

// ** Card Images
import jcbCC from '@src/assets/images/icons/payments/jcb-cc.png'
import amexCC from '@src/assets/images/icons/payments/amex-cc.png'
import uatpCC from '@src/assets/images/icons/payments/uatp-cc.png'
import visaCC from '@src/assets/images/icons/payments/visa-cc.png'
import dinersCC from '@src/assets/images/icons/payments/diners-cc.png'
import maestroCC from '@src/assets/images/icons/payments/maestro-cc.png'
import discoverCC from '@src/assets/images/icons/payments/discover-cc.png'
import mastercardCC from '@src/assets/images/icons/payments/mastercard-cc.png'


const ToastContent = ({ t, name, isError, message }) => {
  return (
    <div className="d-flex">
      <div className="d-flex flex-column">
        <div className="d-flex justify-content-between">
          <h6>{name}</h6>
          <X
            size={12}
            className="cursor-pointer"
            onClick={() => toast.dismiss(t.id)}
          />
        </div>
        {
          isError ? <span>
            You already have bank details.
          </span> : <span>
            {message}
          </span>
        }

      </div>
    </div>
  )
}

const BankDetails = ({ data }) => {
  // ** Hooks
  const defaultValues = {
    first_name: data.first_name,
    last_name: data.last_name,
    email: data.email,
    phone_number: data.phone_number,
  }

  const [accountData, setAccountData] = useState(null)
  const [cardType, setCardType] = useState('')

  const cardsObj = {
    jcb: jcbCC,
    uatp: uatpCC,
    visa: visaCC,
    amex: amexCC,
    diners: dinersCC,
    maestro: maestroCC,
    discover: discoverCC,
    mastercard: mastercardCC
  }


  useEffect(() => {
    getAchRelationship().then((res) => {
      if (res.data.length === 0) {
      } else {
        const data = res.data[0]
        setAccountData({
          bank_account_number: data.bank_account_number,
          bank_routing_number: data.bank_routing_number,
          bank_account_type: data.bank_account_type,
          status: data.status,
        })
      }

    })
  }, [])

  const {
    control,
    setError,
    handleSubmit,
    formState: { errors }
  } = useForm({ defaultValues })
  // ** States
  const onSubmit = (formData) => {
    if (Object.values(formData).every((field) => field.length > 0)) {
      createACHRel(formData).then(() => {
        setAccountData(null)
        toast((t) => (
          <ToastContent
            t={t}
            name={'Success'}
            message='Bank details added'
          />
        ))
      }).catch(() => {
        toast((t) => (
          <ToastContent
            t={t}
            name={'Error'}
            isError={true}
          />
        ))
      })
      return null
    } else {
      for (const key in formData) {
        if (formData[key].length === 0) {
          setError(key, {
            type: 'manual'
          })
        }
      }
    }
  }

  return (
    <Fragment>
      <Card>
        <CardHeader className="border-bottom">
          <CardTitle tag="h4">Bank Details</CardTitle>
        </CardHeader>
        <CardBody className="py-2 my-25">
          {accountData && <>
            <Row>
              <Col sm="6" className="mb-1">
                <p>Bank account number: {accountData.bank_account_number}</p>
              </Col>
              <Col sm="6" className="mb-1">
                <p>Bank routing number: {accountData.bank_routing_number}</p>
              </Col>
            </Row>
            <Row>
              <Col sm="6" className="mb-1">
                <p>Bank account type: {accountData.bank_account_type}</p>
              </Col>
              <Col sm="6" className="mb-1">
                <p>Status: {accountData.status}</p>
              </Col>
            </Row>
          </>

          }
          <Form className="mt-2 pt-50" onSubmit={handleSubmit(onSubmit)}>
            <Row>
              <Col sm="6" className="mb-1">
                <Label className="form-label" for="bank_account_number">
                  Account number
                </Label>
                <InputGroup className='input-group-merge'>
                  <Controller
                    name="bank_account_number"
                    control={control}
                    render={({ field }) => (
                      <Cleave
                        id="bank_account_number"
                        placeholder='1234 5678 9010 1112'
                        {...field}
                        disabled={accountData?.bank_account_number}
                        className={classNames('form-control', { 'is-invalid': errors.bank_account_number })}
                        options={{
                          creditCard: true,
                          onCreditCardTypeChanged: type => {
                            setCardType(type)
                          }
                        }}
                      />
                    )}
                  />
                  {cardType !== '' && cardType !== 'unknown' ? (
                    <InputGroupText className='cursor-pointer p-25'>
                      <img height='24' alt='card-type' src={cardsObj[cardType]} />
                    </InputGroupText>
                  ) : null}
                  {errors && errors.bank_account_number && (
                    <FormFeedback>Please enter a account number</FormFeedback>
                  )}
                </InputGroup>
              </Col>
              <Col sm="6" className="mb-1">
                <Label className="form-label" for="bank_routing_number">
                  Routing number
                </Label>
                <Controller
                  name="bank_routing_number"
                  control={control}
                  render={({ field }) => (
                    <Input
                    disabled={accountData?.bank_routing_number}
                      id="bank_routing_number"
                      invalid={errors.bank_routing_number && true}
                      {...field}
                    />
                  )}
                />
                {errors.bank_routing_number && (
                  <FormFeedback>
                    Please enter a valid routing number
                  </FormFeedback>
                )}
              </Col>
              <Col className="mt-2" sm="12">
                <Button type="submit" className="me-1" color="primary">
                  Save changes
                </Button>
                <Button color="secondary" outline>
                  Discard
                </Button>
              </Col>
            </Row>
          </Form>
        </CardBody>
      </Card>
    </Fragment>
  )
}

export default BankDetails
