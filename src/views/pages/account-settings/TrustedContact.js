// ** React Imports
import { Fragment, useState } from 'react'

// ** Third Party Components
import Select from 'react-select'
import Cleave from 'cleave.js/react'
import { useForm, Controller } from 'react-hook-form'
import 'cleave.js/dist/addons/cleave-phone.us'

// ** Reactstrap Imports
import {
  Row,
  Col,
  Form,
  Card,
  Input,
  Label,
  Button,
  CardBody,
  CardTitle,
  CardHeader,
  FormFeedback,
} from 'reactstrap'

// ** Utils
// import { selectThemeColors } from '@utils'

// ** Demo Components
import DeleteAccount from './DeleteAccount'
import { updateDetail } from '../../../utility/Services'
import { X } from 'react-feather'
import toast from 'react-hot-toast'

import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'


import '@styles/react/libs/react-select/_react-select.scss'

const ToastContent = ({ t, name, isError }) => {
  return (
    <div className="d-flex">
      <div className="d-flex flex-column">
        <div className="d-flex justify-content-between">
          <h6>{name}</h6>
          <X
            size={12}
            className="cursor-pointer"
            onClick={() => toast.dismiss(t.id)}
          />
        </div>
        {
          isError ? <span>
            Something happened on the server
          </span> : <span>
            Data changed successfully
          </span>
        }

      </div>
    </div>
  )
}

const TrustedContact = ({ data }) => {
  // ** Hooks
  const defaultValues = {
    trusted_contact_first_name: data.trusted_contact_first_name,
    trusted_contact_last_name: data.trusted_contact_last_name,
    trusted_contact_email: data.trusted_contact_email,
    trusted_contact_phone_number: data.trusted_contact_phone_number,
  }

  const {
    control,
    setError,
    reset,
    handleSubmit,
    formState: { errors, dirtyFields }
  } = useForm({ defaultValues })
  // ** States
  const onSubmit = (formData) => {
    const modifiedData = {}
    for (const [key, value] of Object.entries(formData)) {
      if (dirtyFields[key]) {
        modifiedData[key] = value
      }
    }
    if (
      Object.entries(formData).every((field) => {
        return field[1] || field[1]?.length > 0 || field[1] === 0
      }
      )
    ) {
      updateDetail(data.id, modifiedData).then(() => {
        toast((t) => (
          <ToastContent
            t={t}
            name={'Success'}
          />
        ))
      }).catch(() => {
        toast((t) => (
          <ToastContent
            t={t}
            name={'Error'}
            isError={true}
          />
        ))
      })
      return null
    } else {
      for (const key in formData) {
        if (formData[key].length === 0) {
          setError(key, {
            type: 'manual'
          })
        }
      }
    }
  }

  return (
    <Fragment>
      <Card>
        <CardHeader className="border-bottom">
          <CardTitle tag="h4">Trusted contact</CardTitle>
        </CardHeader>
        <CardBody className="py-2 my-25">
          <Form className="" onSubmit={handleSubmit(onSubmit)}>
            <Row>
              <Col sm="6" className="mb-1">
                <Label className="form-label" for="trusted_contact_first_name">
                  Name
                </Label>
                <Controller
                  name="trusted_contact_first_name"
                  control={control}
                  render={({ field }) => (
                    <Input
                      id="trusted_contact_first_name"
                      placeholder="Kevin"
                      invalid={errors.trusted_contact_first_name && true}
                      {...field}
                    />
                  )}
                />
                {errors.trusted_contact_first_name && (
                  <FormFeedback>
                    Please enter a valid name
                  </FormFeedback>
                )}
              </Col>
              <Col sm="6" className="mb-1">
                <Label className="form-label" for="trusted_contact_last_name">
                  Last name
                </Label>
                <Controller
                  name="trusted_contact_last_name"
                  control={control}
                  render={({ field }) => (
                    <Input
                      id="trusted_contact_last_name"
                      placeholder="Smith"
                      invalid={errors.trusted_contact_last_name && true}
                      {...field}
                    />
                  )}
                />
                {errors.trusted_contact_last_name && (
                  <FormFeedback>
                    Please enter a valid last name
                  </FormFeedback>
                )}
              </Col>
              <Col sm="6" className="mb-1">
                <Label className="form-label" for="trusted_contact_email">
                  Email
                </Label>
                <Controller
                  name="trusted_contact_email"
                  control={control}
                  render={({ field }) => (
                    <Input
                      id="trusted_contact_email"
                      placeholder="john@example.com"
                      invalid={errors.trusted_contact_email && true}
                      {...field}
                    />
                  )}
                />
                {errors && errors.trusted_contact_email && (
                  <FormFeedback>Please enter a valid email</FormFeedback>
                )}
              </Col>
              <Col sm="6" className="mb-1">
                <Label className="form-label" for="trusted_contact_phone_number">
                  Phone number
                </Label>
                <Controller
                  name="trusted_contact_phone_number"
                  control={control}
                  render={({ field }) => (
                    <Input
                      id="trusted_contact_phone_number"
                      placeholder="8 700 123 4567"
                      invalid={errors.trusted_contact_phone_number && true}
                      {...field}
                      minLength={11}
                    />
                  )}
                />
                {errors.trusted_contact_phone_number && (
                  <FormFeedback>Please enter a valid phone number</FormFeedback>
                )}
              </Col>
              <Col className="mt-2" sm="12">
                <Button type="submit" className="me-1" color="primary">
                  Save changes
                </Button>
                <Button type="button" color="secondary" outline>
                  Discard
                </Button>
              </Col>
            </Row>
          </Form>
        </CardBody>
      </Card>
    </Fragment>
  )
}

export default TrustedContact
