// ** Reactstrap Imports
import { Nav, NavItem, NavLink } from 'reactstrap'

// ** Icons Imports
import { User, CreditCard, DollarSign, AtSign } from 'react-feather'

const Tabs = ({ activeTab, toggleTab }) => {
  return (
    <Nav pills className='mb-2'>
      <NavItem>
        <NavLink active={activeTab === '1'} onClick={() => toggleTab('1')}>
          <User size={18} className='me-50' />
          <span className='fw-bold'>Information</span>
        </NavLink>
      </NavItem>
      {/* <NavItem>
        <NavLink active={activeTab === '2'} onClick={() => toggleTab('2')}>
          <CreditCard size={18} className='me-50' />
          <span className='fw-bold'>Financial</span>
        </NavLink>
      </NavItem> */}
      <NavItem>
        <NavLink active={activeTab === '2'} onClick={() => toggleTab('2')}>
          <DollarSign size={18} className='me-50' />
          <span className='fw-bold'>Bank details</span>
        </NavLink>
      </NavItem>
      <NavItem>
        <NavLink active={activeTab === '3'} onClick={() => toggleTab('3')}>
          <AtSign size={18} className='me-50' />
          <span className='fw-bold'>Trusted contact</span>
        </NavLink>
      </NavItem>

    </Nav>
  )
}

export default Tabs
