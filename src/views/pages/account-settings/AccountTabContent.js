// ** React Imports
import { Fragment, useState } from 'react'

// ** Third Party Components
import Select from 'react-select'
import Cleave from 'cleave.js/react'
import { useForm, Controller } from 'react-hook-form'
import 'cleave.js/dist/addons/cleave-phone.us'

// ** Reactstrap Imports
import {
  Row,
  Col,
  Form,
  Card,
  Input,
  Label,
  Button,
  CardBody,
  CardTitle,
  CardHeader,
  FormFeedback,
} from 'reactstrap'

// ** Utils
// import { selectThemeColors } from '@utils'

// ** Demo Components
import DeleteAccount from './DeleteAccount'
import { updateDetail } from '../../../utility/Services'
import { X } from 'react-feather'
import toast from 'react-hot-toast'

// ** Third Party Components
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'

import { selectThemeColors } from '@utils'

import '@styles/react/libs/react-select/_react-select.scss'
import countryOptions from '../authentication/register-multi-steps/countryOptions'
import { useEffect } from 'react'
import Popover from './Popover'

const taxIdTypeOptions = [
  { value: 'USA_SSN', label: 'USA SSN' },
  { value: 'ARG_AR_CUIT', label: 'ARG AR CUIT' },
  { value: 'AUS_TFN', label: 'AUS TFN' },
  { value: 'AUS_ABN', label: 'AUS ABN' },
  { value: 'BOL_NIT', label: 'BOL NIT' },
  { value: 'BRA_CPF', label: 'BRA CPF' },
  { value: 'CHL_RUT', label: 'CHL RUT' },
  { value: 'COL_NIT', label: 'COL NIT' },
  { value: 'CRI_NITE', label: 'CRI NITE' },
  { value: 'DEU_TAX_ID', label: 'DEU TAX ID' },
  { value: 'DOM_RNC', label: 'DOM RNC' },
  { value: 'ECU_RUC', label: 'ECU RUC' },
  { value: 'FRA_SPI', label: 'FRA SPI' },
  { value: 'GBR_UTR', label: 'GBR UTR' },
  { value: 'GBR_NINO', label: 'GBR NINO' },
  { value: 'GTM_NIT', label: 'GTM NIT' },
  { value: 'HND_RTN', label: 'HND RTN' },
  { value: 'IDN_KTP', label: 'IDN_KTP' },
  { value: 'IND_PAN', label: 'IND PAN' },
  { value: 'ISR_TAX_ID', label: 'ISR TAX ID' },
  { value: 'ITA_TAX_ID', label: 'ITA TAX ID' },
  { value: 'JPN_TAX_ID', label: 'JPN TAX ID' },
  { value: 'MEX_RFC', label: 'MEX RFC' },
  { value: 'NIC_RUC', label: 'NIC RUC' },
  { value: 'NLD_TIN', label: 'NLD TIN' },
  { value: 'PAN_RUC', label: 'PAN RUC' },
  { value: 'PER_RUC', label: 'PER RUC' },
  { value: 'PRY_RUC', label: 'PRY RUC' },
  { value: 'SGP_NRIC', label: 'SGP NRIC' },
  { value: 'SGP_FIN', label: 'SGP FIN' },
  { value: 'SGP_ASGD', label: 'SGP ASGD' },
  { value: 'SGP_ITR', label: 'SGP ITR' },
  { value: 'SLV_NIT', label: 'SLV NIT' },
  { value: 'SWE_TAX_ID', label: 'SWE TAX ID' },
  { value: 'URY_RUT', label: 'URY RUT' },
  { value: 'VEN_RIF', label: 'VEN RIF' },
  { value: 'NOT_SPECIFIED', label: 'NOT SPECIFIED' },
]

const incomeOptions = [
  { value: null, label: 'Choose an option' },
  { value: [0, 20000], label: '0 - $20,000' },
  { value: [20000, 49999], label: '$20,000 - $49,999' },
  { value: [50000, 99999], label: '$50,000 - $99,999' },
  { value: [100000, 499999], label: '$100,000 - $499,999' },
  { value: [500000, 999999], label: '$500,000 - $999,999' },
  { value: [1000000, 9999999], label: '$1,000,000 +' }
]

const liquidAssets = [
  { value: [0, 20000], label: '0 - $20,000' },
  { value: [20000, 49999], label: '$20,000 - $49,999' },
  { value: [50000, 99999], label: '$50,000 - $99,999' },
  { value: [100000, 499999], label: '$100,000 - $499,999' },
  { value: [500000, 999999], label: '$500,000 - $999,999' },
  { value: [1000000, 9999999], label: '$1,000,000 +' }
]

const fundingSource = [
  { value: 'employment_income', label: 'Employment income' },
  { value: 'investments', label: 'Investments' },
  { value: 'inheritance', label: 'Inheritance' },
  { value: 'business_income', label: 'Business income' },
  { value: 'savings', label: 'Savings' },
  { value: 'family ', label: 'Family' }
]

const employmentStatus = [
  { value: 'employed', label: 'Employed' },
  { value: 'unemployed', label: 'Unemployed' },
  { value: 'student', label: 'Student' },
  { value: 'retired ', label: 'Retired' }
]

const ToastContent = ({ t, name, isError }) => {
  return (
    <div className="d-flex">
      <div className="d-flex flex-column">
        <div className="d-flex justify-content-between">
          <h6>{name}</h6>
          <X
            size={12}
            className="cursor-pointer"
            onClick={() => toast.dismiss(t.id)}
          />
        </div>
        {
          isError ? <span>
            Something happened on the server
          </span> : <span>
            Data changed successfully
          </span>
        }

      </div>
    </div>
  )
}

const getSelectData = (array, value) => {
  const data = array.filter((item) => {
    return item.value === value
  })[0]
  return data
}

const getSelectNumberData = (incomeOptions, maxValue) => {
  switch (maxValue) {
    case null:
      return { value: [null, null], label: 'Choose an option' }
    case 20000:
      return { value: [0, 20000], label: '0 - $20,000' }
    case 49999:
      return { value: [20000, 49999], label: '$20,000 - $49,999' }
    case 99999:
      return { value: [50000, 99999], label: '$50,000 - $99,999' }
    case 499999:
      return { value: [100000, 499999], label: '$100,000 - $499,999' }
    case 999999:
      return { value: [500000, 999999], label: '$500,000 - $999,999' }
    case 9999999:
      return { value: [1000000, 9999999], label: '$1,000,000 +' }
  }
}

const AccountTabs = ({ data }) => {
  // ** Hooks
  const defaultValues = {
    // email: data.email,
    street_address: data.street_address,
    city: data.city,
    phone_number: data.phone_number,
    state: data.state,
    postal_code: data.postal_code,
    // country: data.country,
    unit_apt: data.unit_apt,
    annual_income_min: data.annual_income_min,
    annual_income_max: data.annual_income_max,
    liquid_net_worth_min: data.liquid_net_worth_min,
    liquid_net_worth_max: data.liquid_net_worth_max,
    funding_source: data.funding_source,
    employment_status: data.employment_status,
    employer_name: data.employer_name,
    employer_address: data.employer_address,
    employment_position: data.employment_position,
    // is_affiliated_exchange_or_finra: data.is_affiliated_exchange_or_finra,
    // is_control_person: data.is_control_person,
    // is_politically_exposed: data.is_politically_exposed,
    // immediate_family_exposed: data.immediate_family_exposed,

  }

  const [taxIdType, setTaxIdType] = useState(getSelectData(taxIdTypeOptions, data.tax_id_type).label)

  const [annual, setAnnual] = useState(getSelectNumberData(incomeOptions, data.annual_income_max))
  const [liquid, setLiquid] = useState(getSelectNumberData(incomeOptions, data.liquid_net_worth_max))
  const [funding, setFunding] = useState(getSelectData(fundingSource, data.funding_source))
  const [employment, setEmployment] = useState(getSelectData(employmentStatus, data.employment_status))

  const optionalFields = ['annual_income_max', 'annual_income_min', 'employer_name', 'employer_address', 'employment_position']

  const {
    control,
    setError,
    reset,
    handleSubmit,
    formState: { errors, dirtyFields }
  } = useForm({ defaultValues })
  // ** States
  const onSubmit = (formData) => {
    const modifiedData = {}
    for (const [key, value] of Object.entries(formData)) {
      if (dirtyFields[key]) {
        modifiedData[key] = value
      }
    }
    if (
      Object.entries(formData).every((field) => {
        const isOptional = optionalFields.includes(field[0])
        return isOptional || field[1] || field[1]?.length > 0 || field[1] === 0
      }
      )
    ) {
      if (!annual.value) {
        modifiedData.annual_income_min = null
        modifiedData.annual_income_max = null
      } else {
        modifiedData.annual_income_min = annual.value[0]
        modifiedData.annual_income_max = annual.value[1]
      }
      modifiedData.liquid_net_worth_min = liquid.value[0]
      modifiedData.liquid_net_worth_max = liquid.value[1]
      updateDetail(data.id, modifiedData).then(() => {
        toast((t) => (
          <ToastContent
            t={t}
            name={'Success'}
          />
        ))
      }).catch(() => {
        toast((t) => (
          <ToastContent
            t={t}
            name={'Error'}
            isError={true}
          />
        ))
      })
      return null
    } else {
      for (const key in formData) {
        if (!optionalFields.includes(key) && formData[key]?.length === 0) {
          setError(key, {
            type: 'manual'
          })
        }
      }
    }
  }

  return (
    <Fragment>
      <Card>
        <CardHeader className="border-bottom">
          <CardTitle tag="h4">Profile Details</CardTitle>
        </CardHeader>
        <CardBody className="py-2 my-25">
          <Form className="mt-2 pt-50" onSubmit={handleSubmit(onSubmit)}>
            <Row>
              <Col sm="6" className="mb-1">
                <Label className="form-label" for="first_name">
                  First name
                </Label>
                <Input
                  id="first_name"
                  placeholder="John"
                  disabled
                  defaultValue={data.first_name}
                />
              </Col>
              <Col sm="6" className="mb-1">
                <Label className="form-label" for="last_name">
                  Last name
                </Label>
                <Input
                  id="last_name"
                  placeholder="Doe"
                  disabled
                  defaultValue={data.last_name}
                />
              </Col>
              <Col sm="6" className="mb-1">
                <Label className="form-label" for="date_of_birth">
                  Date of birth
                </Label>
                <Input
                  id="date_of_birth"
                  placeholder="John"
                  disabled
                  defaultValue={data.date_of_birth}
                />
              </Col>
              <Col sm="6" className="mb-1">
                <Label className="form-label" for="tax_id">
                  Tax id
                </Label>
                <Input
                  id="tax_id"
                  placeholder="123-123"
                  disabled
                  defaultValue={data.tax_id}
                />
              </Col>
              <Col sm="6" className="mb-1">
                <Label className="form-label" for="tax_id_type">
                  Tax id type
                </Label>
                <Input
                  id="tax_id_type"
                  placeholder=""
                  disabled
                  defaultValue={taxIdType}
                />
              </Col>
              <Col sm="6" className="mb-1">
                <Label className="form-label" for="email">
                  Email
                </Label>
                <Input
                  id="email"
                  placeholder=""
                  disabled
                  defaultValue={data.email}
                />
              </Col>
              {/* <Col sm="6" className="mb-1">
                <Label className="form-label" for="email">
                  Email
                </Label>
                <Controller
                  name="email"
                  control={control}
                  render={({ field }) => (
                    <Input
                      id="email"
                      placeholder="john@example.com"
                      invalid={errors.email && true}
                      {...field}
                    />
                  )}
                />
                {errors && errors.email && (
                  <FormFeedback>Please enter a valid email</FormFeedback>
                )}
              </Col> */}
              <Col sm="6" className="mb-1">
                <Label className="form-label" for="country">
                  Country of birth
                </Label>
                <Input
                  id="country"
                  placeholder="John"
                  disabled
                  defaultValue={data.country}
                />
              </Col>
              <Col sm="6" className="mb-1">
                <Label className="form-label" for="country_of_citizenship">
                  Country of citizenship
                </Label>
                <Input
                  id="country_of_citizenship"
                  placeholder="Doe"
                  disabled
                  defaultValue={data.country_of_citizenship}
                />
              </Col>
              <Col sm="6" className="mb-1">
                <Label className="form-label" for="country_of_tax_residence">
                  Country of tax residence
                </Label>
                <Input
                  id="country_of_tax_residence"
                  placeholder="Doe"
                  disabled
                  defaultValue={data.country_of_tax_residence}
                />
              </Col>
            </Row>
            <hr />
            <Row>

              <Col sm="6" className="mb-1">
                <Label className="form-label" for="street_address">
                  Street address
                </Label>
                <Controller
                  name="street_address"
                  control={control}
                  render={({ field }) => (
                    <Input
                      id="street_address"
                      placeholder="Wall street"
                      invalid={errors.street_address && true}
                      {...field}
                    />
                  )}
                />
                {errors.street_address && (
                  <FormFeedback>
                    Please enter a valid street address
                  </FormFeedback>
                )}
              </Col>
              <Col sm="6" className="mb-1">
                <Label className="form-label" for="phone_number">
                  Phone number
                </Label>
                <Controller
                  name="phone_number"
                  control={control}
                  render={({ field }) => (
                    <Input
                      id="phone_number"
                      placeholder="8 700 123 4567"
                      invalid={errors.phone_number && true}
                      {...field}
                      minLength={11}
                    />
                  )}
                />
                {errors.phone_number && (
                  <FormFeedback>Please enter a valid phone number</FormFeedback>
                )}
              </Col>

              <Col sm="6" className="mb-1">
                <Label className="form-label" for="unit_apt">
                  Unit / Apt #
                </Label>
                <Controller
                  name="unit_apt"
                  control={control}
                  render={({ field }) => (
                    <Input
                      id="unit_apt"
                      placeholder="123456"
                      invalid={errors.unit_apt && true}
                      {...field}
                    />
                  )}
                />
                {errors.unit_apt && (
                  <FormFeedback>Please enter a valid unit / apt #</FormFeedback>
                )}
              </Col>
              <Col sm="6" className="mb-1">
                <Label className="form-label" for="city">
                  City
                </Label>
                <Controller
                  name="city"
                  control={control}
                  render={({ field }) => (
                    <Input
                      id="city"
                      placeholder="Almaty"
                      invalid={errors.city && true}
                      {...field}
                    />
                  )}
                />
                {errors.city && (
                  <FormFeedback>Please enter a valid city</FormFeedback>
                )}
              </Col>
              <Col sm="6" className="mb-1">
                <Label className="form-label" for="state">
                  State
                </Label>
                <Controller
                  name="state"
                  control={control}
                  render={({ field }) => (
                    <Input
                      id="state"
                      placeholder="Almaty"
                      invalid={errors.state && true}
                      {...field}
                    />
                  )}
                />
                {errors.state && (
                  <FormFeedback>Please enter a valid state</FormFeedback>
                )}
              </Col>
              <Col sm="6" className="mb-1">
                <Label className="form-label" for="postal_code">
                  Postal code
                </Label>
                <Controller
                  name="postal_code"
                  control={control}
                  render={({ field }) => (
                    <Input
                      id="postal_code"
                      placeholder="123456"
                      invalid={errors.postal_code && true}
                      {...field}
                    />
                  )}
                />
                {errors.postal_code && (
                  <FormFeedback>Please enter a valid postal code</FormFeedback>
                )}
              </Col>

              {/* <Col className='mb-1' md='4'>
                <Label className='form-label'>Сountry</Label>
                <Select
                  theme={selectThemeColors}
                  className='react-select'
                  classNamePrefix='select'
                  value={country}
                  options={countryOptions}
                  isClearable={false}
                  onChange={(data) => { setCountry(data) }}
                />
                {errors.country && <FormFeedback>{errors.country.message}</FormFeedback>}
              </Col>
              <Col className='mb-1' md='4'>
                <Label className='form-label'>Сountry of citizenship</Label>
                <Select
                  theme={selectThemeColors}
                  className='react-select'
                  classNamePrefix='select'
                  value={citizenship}
                  options={countryOptions}
                  isClearable={false}
                  onChange={(data) => { setCitizenship(data) }}
                />
                {errors.country_of_citizenship && <FormFeedback>{errors.country_of_citizenship.message}</FormFeedback>}
              </Col>
              <Col className='mb-1' md='4'>
                <Label className='form-label'>Сountry of tax residence</Label>
                <Select
                  theme={selectThemeColors}
                  className='react-select'
                  classNamePrefix='select'
                  value={taxResidence}
                  options={countryOptions}
                  isClearable={false}
                  onChange={(data) => { setTaxResidence(data) }}
                />
                {errors.country_of_tax_residence && <FormFeedback>{errors.country_of_tax_residence.message}</FormFeedback>}
              </Col> */}

              <Col className='mb-1' md='6' sm='12'>
                <Label className='form-label'>Account funding source</Label>
                <Select
                  theme={selectThemeColors}
                  className='react-select'
                  classNamePrefix='select'
                  value={funding}
                  options={fundingSource}
                  isClearable={false}
                  onChange={(data) => { setFunding(data) }}
                />
              </Col>
              <Col className='mb-1' md='6' sm='12'>
                <Label className='form-label'>Employment Status</Label>
                <Select
                  theme={selectThemeColors}
                  className='react-select'
                  classNamePrefix='select'
                  value={employment}
                  options={employmentStatus}
                  isClearable={false}
                  onChange={(data) => { setEmployment(data) }}
                />
              </Col>

              <Col className='mb-1' md='6' sm='12'>
                <span className='d-flex justify-content-between align-items-center'>
                  <Label className='form-label'>Annual household income <span className='text-secondary'>(optional)</span></Label>
                  <Popover target={'annual'} text={'Annual household income includes income from sources such as employment, alimony, social security, investment income, etc.'}></Popover>
                </span>
                <Select
                  theme={selectThemeColors}
                  className='react-select'
                  classNamePrefix='select'
                  value={annual}
                  options={incomeOptions}
                  isClearable={false}
                  onChange={(data) => { setAnnual(data) }}
                />
              </Col>
              <Col className='mb-1' md='6' sm='12'>
                <span className='d-flex justify-content-between align-items-center'>
                  <Label className='form-label'>Investible / Liquid assets</Label>
                  <Popover target={'liquid'} text={'Investible / Liquid assets is your net worth minus assets that cannot be converted quickly and easily into cash, such as real estate, business equity, personal property and automobiles, expected inheritances, assets earmarked for other purposes, and investments or accounts subject to substantial penalties if they were sold or if assets were withdrawn from them.'}></Popover>
                </span>
                <Select
                  theme={selectThemeColors}
                  className='react-select'
                  classNamePrefix='select'
                  value={liquid}
                  options={liquidAssets}
                  isClearable={false}
                  onChange={(data) => { setLiquid(data) }}
                />
                {errors.liquid_net_worth_min && <small className='mx-1 text-danger opacity-75'>{errors.liquid_net_worth_min.message}</small>}
              </Col>

              <div className='divider'>
                <div className='divider-text'>Employment</div>
              </div>
              <Col md='4' className='mb-1'>
                <Label className='form-label' for='employer_name'>
                  Name of Employer <span className='text-secondary'>(optional)</span>
                </Label>
                <Controller
                  id='employer_name'
                  name='employer_name'
                  control={control}
                  render={({ field }) => <Input name='employer_name' invalid={errors.employer_name && true} {...field} />}
                />
                {errors.employer_name && <FormFeedback>{errors.employer_name.message}</FormFeedback>}
              </Col>
              <Col md='4' className='mb-1'>
                <Label className='form-label' for='employer_address'>
                  Employer address <span className='text-secondary'>(optional)</span>
                </Label>
                <Controller
                  id='employer_address'
                  name='employer_address'
                  control={control}
                  render={({ field }) => <Input invalid={errors.employer_address && true} {...field} />}
                />
                {errors.employer_address && <FormFeedback>{errors.employer_address.message}</FormFeedback>}
              </Col>
              <Col md='4' className='mb-1'>
                <Label className='form-label' for='employment_position'>
                  Occupation / Job Title <span className='text-secondary'>(optional)</span>
                </Label>
                <Controller
                  id='employment_position'
                  name='employment_position'
                  control={control}
                  render={({ field }) => <Input invalid={errors.employment_position && true} {...field} />}
                />
                {errors.employment_position && <FormFeedback>{errors.employment_position.message}</FormFeedback>}
              </Col>
              <Col className="mt-2" sm="12">
                <Button type="submit" className="me-1" color="primary">
                  Save changes
                </Button>
                <Button type="button" color="secondary" outline>
                  Discard
                </Button>
              </Col>
            </Row>
          </Form>
        </CardBody>
      </Card>
      <DeleteAccount />
    </Fragment>
  )
}

export default AccountTabs
