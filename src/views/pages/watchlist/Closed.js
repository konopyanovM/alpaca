// ** Icons Imports
import { Card } from 'reactstrap'

const Closed = () => {


  return (
    <Card className='card-company-table p-3 d-flex'>
      <h3 className='text-center'>Market is closed</h3>
    </Card>
  )
}

export default Closed
