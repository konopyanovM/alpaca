// ** React Imports
import { useContext, useState } from 'react'

// ** Reactstrap Imports
import { Row, Col, Card, CardBody, Input, Button, Label, Form, FormFeedback } from 'reactstrap'

// ** Context
import { ThemeColors } from '@src/utility/context/ThemeColors'

// ** Demo Components
import CompanyTable from './CompanyTable'

// ** Styles
import '@styles/react/libs/charts/apex-charts.scss'
import '@styles/base/pages/dashboard-ecommerce.scss'
import { getStockBars } from '../../../utility/Services'
import Closed from './Closed'
import { Controller, useForm } from 'react-hook-form'
import toast from 'react-hot-toast'
import { X } from 'react-feather'
import { getStockDate } from '../../../utility/Utils'

const EcommerceDashboard = () => {
  // ** Context
  const { colors } = useContext(ThemeColors)

  const ToastContent = ({ t, name, isError, message }) => {
    return (
      <div className="d-flex">
        <div className="d-flex flex-column">
          <div className="d-flex justify-content-between">
            <h6>{name}</h6>
            <X
              size={12}
              className="cursor-pointer"
              onClick={() => toast.dismiss(t.id)}
            />
          </div>
          {
            isError ? <span>
              Something went wrong.
            </span> : <span>
              {message}
            </span>
          }

        </div>
      </div>
    )
  }

  const [start, end] = getStockDate()

  const [isClosed, setClosed] = useState(false)

  const getStocks = async () => {
    const response = await getStockBars({
      symbol: 'AAPL',
      start,
      end,
    })
    if (response.data.message === 'Exchange closed') {
      setClosed(true)
    }
  }
  getStocks()

  const {
    control,
    setError,
    handleSubmit,
    reset,
    formState: { errors }
  } = useForm({
    defaultValues: { symbol: '' }
  })

  const [stocks, setStocks] = useState(JSON.parse(localStorage.getItem('assets')) || [])

  const onSubmit = (data) => {
    if (data.symbol.length === 0) {
      setError('symbol', {
        type: 'manual',
        message: "Please enter a valid asset"
      })
      return
    }
    const symbol = data.symbol.toUpperCase()
    getStockBars({
      symbol,
      start,
      end
    }).then((res) => {
      if (res.data.message) {
        setError('symbol', {
          type: 'manual',
          message: "Can't find such an asset"
        })
      } else {
        const assets = JSON.parse(localStorage.getItem('assets')) || []
        if (assets.includes(symbol)) {
          toast((t) => (
            <ToastContent
              t={t}
              name={'Error'}
              message='You have already added this asset'
            />
          ))
        } else {
          assets.push(res.data[symbol][0].symbol)
          localStorage.setItem('assets', JSON.stringify(assets))
          setStocks(assets)
          reset()
          toast((t) => (
            <ToastContent
              t={t}
              name={'Success'}
              message='Asset successfully added'
            />
          ))
        }
      }
    })
  }

  const removeHandler = (symbol) => {
    const assets = JSON.parse(localStorage.getItem('assets')) || []
    if (assets.includes(symbol)) {
      const newAssets = assets.filter(e => e.toUpperCase() !== symbol.toUpperCase());
      localStorage.setItem('assets', JSON.stringify(newAssets))
      setStocks(newAssets)
      toast((t) => (
        <ToastContent
          t={t}
          name={'Success'}
          message='Asset successfully removed'
        />
      ))
    }
  }

  return (
    <div id='dashboard-ecommerce'>
      <Row className='match-height'>
        <Col lg='12' xs='12'>
          <Card>
            <CardBody>
              <Form onSubmit={handleSubmit(onSubmit)}>
                <Row>
                  <Label className='form-label' for='symbol'>
                    Add asset
                  </Label>
                  <Col md='11' className='mb-1'>
                    <Controller
                      id='symbol'
                      name='symbol'
                      control={control}
                      render={({ field }) => <Input placeholder='Asset title' invalid={errors.symbol && true} {...field} />}
                    />
                    {errors.symbol && <FormFeedback>{errors.symbol.message}</FormFeedback>}
                  </Col>
                  <Col md='1' className='mb-1'>
                    <Button type='submit' className='me-1' color='success'>
                      Add
                    </Button>
                    {/* <Button className='me-1' color='danger' onClick={removeHandler}>
                      Remove
                    </Button> */}
                  </Col>
                </Row>
              </Form>
            </CardBody>
          </Card>
          {isClosed
            ? <Closed></Closed>
            : <CompanyTable stocks={stocks} setStocks={setStocks} removeHandler={removeHandler} />
          }
        </Col>
      </Row>
    </div>
  )
}

export default EcommerceDashboard
