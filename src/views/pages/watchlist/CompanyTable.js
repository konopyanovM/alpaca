// ** Icons Imports
import { useState, useEffect } from 'react'
import { TrendingUp, TrendingDown, X } from 'react-feather'
import { Link } from 'react-router-dom'
import { Button, Table, Card, Spinner, CardTitle, CardBody } from 'reactstrap'
import { getBalance, getStockBars } from '../../../utility/Services'
import { getStockDate } from '../../../utility/Utils'

const CompanyTable = ({ stocks, setStocks, removeHandler }) => {
  const [start, end] = getStockDate()

  const [userBalance, setUserBalance] = useState(0)
  const [stockData, setStockData] = useState([])
  const [exchangeClosed, setExchangeClosed] = useState(false)

  useEffect(() => {
    setStockData([])
    stocks.forEach((symbol) => {
      getStockBars({
        symbol,
        start,
        end,
      }).then((res) => {
        const keyName = Object.keys(res.data)[0]
        setStockData((prev) => {
          const array = [...prev]
          array.push(res.data[keyName])
          return array
        })
      })
    })
  }, [stocks])

  useEffect(() => {
    getBalance().then((res) => {
      setUserBalance(res.data.balance)
    })
    if (stockData[0] === 'Exchange closed') setExchangeClosed(true)
  }, [stockData])

  const renderData = () => {
    if (exchangeClosed) {
      return <Col xs={12} className="d-flex justify-content-center p-5 mt-5"><h1>Exchange closed</h1></Col>
    } else if (stockData[0] !== 'Exchange closed') {
      return stockData.map((col) => {
        const getDiff = (high, low) => {
          const change = high / low
          if (change < 1) {
            const diff = (1 - (high / low)).toFixed(3)
            return <span className='text-danger fw-bold'>{diff}%</span>
          }
          if (change === 1) {
            const diff = 0
            return <span className='text-success fw-bold'>{diff}%</span>
          }
          if (change > 1) {
            const diff = ((low / high)).toFixed(3)
            return <span className='text-success fw-bold'>{diff}%</span>
          }
        }

        const renderIcon = (high, low) => {
          const change = high / low
          if (change < 1) {
            return <TrendingDown size={15} className='text-danger' />
          }
          if (change === 1) {
            return <Minus size={15} />
          }
          if (change > 1) {
            return <TrendingUp size={15} className='text-success' />
          }
        }

        const title = col[0].symbol
        const date = col[1]?.timestamp?.split('T')[0] || col[0]?.timestamp?.split('T')[0]
        const tradeCount = col[0].trade_count
        const volume = col[0].volume
        const low = col[0].low
        const high = col[0].high
        const diff = getDiff(high, low)
        const Icon = renderIcon(high, low)
        const open = col[0].open
        const close = col[0].close
        const symbol = col[0].symbol

        return (
          <tr key={title}>
            <td>
              <div className='d-flex align-items-center'>
                <Link to={{
                  pathname: `/stocks/${title.toLowerCase()}`,
                  search: "",
                }}>
                  <div className='fw-bolder'>{title}</div>
                </Link>
              </div>
            </td>
            <td className='text-nowrap'>
              <div className='d-flex flex-column'>
                <span>{volume}</span>
              </div>
            </td>
            <td className='text-nowrap'>
              <div className='d-flex flex-column'>
                <span>{tradeCount}</span>
              </div>
            </td>
            <td className='flex-grow-1'>
              <div className='d-flex align-items-center'>
                <span>{low}</span>
              </div>
            </td>
            <td className='flex-grow-1'>
              <div className='d-flex align-items-center'>
                <span>{high}</span>
              </div>
            </td>
            <td className='flex-grow-1'>
              <div className='d-flex align-items-center'>
                <span>{diff} {Icon}</span>
              </div>
            </td>
            <td className='flex-grow-1'>
              <div className='d-flex align-items-center'>
                <span>{open}</span>
              </div>
            </td>
            <td className='flex-grow-1'>
              <div className='d-flex align-items-center'>
                <span>{close}</span>
              </div>
            </td>
            <td className='flex-grow-1'>
              <div className='d-flex align-items-center'>
                <span>{date}</span>
              </div>
            </td>
            <td className='flex-grow-1'>
              <div className='d-flex align-items-center'>
                <Link to={{
                  pathname: `/stocks/${title.toLowerCase()}`,
                  search: "",
                }}>
                  <Button.Ripple color='info'>Trade</Button.Ripple>
                </Link>
              </div>
            </td>
            <td className='flex-grow-1'>
              <div className='d-flex align-items-center'>
                <Button.Ripple color='danger' onClick={() => removeHandler(symbol.toUpperCase())}>Remove</Button.Ripple>
              </div>
            </td>
          </tr>
        )
      })
    }

  }

  return (
    <Card className='company-table'>
      <CardBody>
        <CardTitle className='mb-0' tag="h4">Watchlist</CardTitle>
      </CardBody>
      {
        stockData.length !== 0 && !(exchangeClosed)
          ? <Table responsive>
            <thead>
              <tr>
                <th>Asset</th>
                <th>Volume</th>
                <th>Trade count</th>
                <th>Low</th>
                <th>High</th>
                <th>Change</th>
                <th>Open</th>
                <th>Close</th>
                <th>Date</th>
                <th></th>
                <th></th>
              </tr>
            </thead>
            <tbody>{renderData()}</tbody>
          </Table>
          : stocks.length !== 0
            ? <div className='py-4 d-flex justify-content-center mb-5'>
              <Spinner />
            </div>
            : <div className='py-4 d-flex align-items-center mb-5 flex-column'>
              <h3>There are no assets in your watchlist.</h3>
              <h3>Add some to start trading!</h3>
            </div>
      }
    </Card>
  )
}

export default CompanyTable
