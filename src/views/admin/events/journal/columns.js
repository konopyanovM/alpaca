import { Badge } from 'reactstrap'


const renderDirection = (type) => {
  if (type === 'INCOMING') return (
    <Badge color='success' className='d-block'>
      <span>{type}</span>
    </Badge>
  )
  if (type === 'OUTCOMING') return (
    <Badge color='danger' className='d-block'>
      <span>{type}</span>
    </Badge>
  )
  return <Badge color='primary' className='d-block'>
    <span>{type}</span>
  </Badge>
}

const renderStatus = (type) => {
  if (type === 'queued') return (
    <Badge color='warning' className='d-block'>
      <span>Queued</span>
    </Badge>
  )
  if (type === 'sent_to_clearing') return (
    <Badge color='info' className='d-block'>
      <span>Sent to clearing</span>
    </Badge>
  )
  if (type === 'executed') return (
    <Badge color='success' className='d-block'>
      <span>Executed</span>
    </Badge>
  )
  if (type === '' || !type) return
  return (
    <Badge color='primary' className='d-block'>
      <span>{type}</span>
    </Badge>
  )
}

const renderDate = (date) => {
  return new Date(date).toLocaleDateString("ru-RU")
}

export const columns = [
  {
    name: 'Journal ID',
    sortable: true,
    sortField: 'first_name',
    selector: row => row.first_name,
    cell: row => {
      return (
        <div className='d-flex justify-content-left align-items-center'>
          <span className='fw-bolder'>{row.journal_id}</span>
        </div>
      )
    }
  },
  {
    name: 'Event ID',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      <span className='fw-bolder'>{row.event_id}</span>
    </div>
  },
  {
    name: 'Entry type',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      <span className='fw-bolder'>{row.entry_type}</span>
    </div>
  },
  {
    name: 'Status from',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {renderStatus(row.status_from)}
    </div>
  },
  {
    name: 'Status to',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {renderStatus(row.status_to)}
    </div>
  },
  {
    name: 'Date',
    cell: row => (
      <div className='column-action'>
        {renderDate(row.at)}
      </div>
    )
  }
]