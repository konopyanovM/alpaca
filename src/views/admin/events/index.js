// ** User List Component
import AccountsTable from './accounts/Table'
import JournalTable from './journal/Table'
import TradeTable from './trade/Table'
import TransferTable from './transfer/Table'

import Breadcrumbs from '@components/breadcrumbs'

// ** React Imports
import { useState, useEffect } from 'react'
// ** Reactstrap Imports
import {
  Button,
  Card, CardBody, Col, Form, FormFeedback, Input, Label, Row, Spinner, TabContent, TabPane,
} from 'reactstrap'

// ** Styles
import '@styles/react/libs/react-select/_react-select.scss'
import '@styles/react/libs/tables/react-dataTable-component.scss'
import { getEvent } from '../../../utility/Services'
import { getStockDate } from '../../../utility/Utils'

import { Controller, useForm } from 'react-hook-form'

// ** Styles
import '@styles/react/apps/app-users.scss'
import Tabs from './Tabs'


const defaultValues = {
  from: '',
  to: ''
}

const UsersList = () => {
  const [since, until] = getStockDate(14)

  const [list, setList] = useState([])
  const [from, setFrom] = useState(since)
  const [to, setTo] = useState(until)
  const [activeTab, setActiveTab] = useState('1')
  const {
    control,
    setError,
    handleSubmit,
    getValues,
    formState: { errors }
  } = useForm({
    defaultValues
  })

  const toggleTab = tab => {
    setActiveTab(tab)
  }

  const onSubmit = (data) => {
    if (data.from === '') {
      setError('from', {
        type: 'manual',
        message: 'Please pick a date'
      })
      return
    }
    if (data.to === '') {
      setError('to', {
        type: 'manual',
        message: 'Please pick a date'
      })
      return
    }
    setFrom(data.from)
    setTo(data.to)
  }


  return <>
    <Breadcrumbs title='Events' data={[{ title: 'Events' }]} />
    <Col xs={12}>
      <Tabs className='mb-2' activeTab={activeTab} toggleTab={toggleTab} />
    </Col>
    <Card>
      <CardBody>
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Row>
            <Col md='5' className='mb-1'>
              <Label className='form-label' for='from'>
                From
              </Label>
              <Controller
                id='from'
                name='from'
                control={control}
                render={({ field }) => <Input type='date' id='from' name='from' invalid={errors.from && true} {...field} />}
              />
              {errors.from && <FormFeedback>{errors.from.message}</FormFeedback>}
            </Col>
            <Col md='5' className='mb-1'>
              <Label className='form-label' for='to'>
                To
              </Label>
              <Controller
                id='to'
                name='to'
                control={control}
                render={({ field }) => <Input type='date' id='to' name='to' invalid={errors.to && true} {...field} />}
              />
              {errors.to && <FormFeedback>{errors.to.message}</FormFeedback>}
            </Col>
            <Col md='2' className='mb-1 d-flex align-items-end'>
              <Button color='success' onSubmit={handleSubmit(onSubmit)}>Submit</Button>
            </Col>
          </Row>
        </Form>
      </CardBody>
    </Card>
    <TabContent activeTab={activeTab}>
      <TabPane tabId='1'>
        <AccountsTable from={from} to={to}></AccountsTable>
      </TabPane>
      <TabPane tabId='2'>
        <JournalTable from={from} to={to}></JournalTable>
      </TabPane>
      <TabPane tabId='3'>
        <TradeTable from={from} to={to}></TradeTable>
      </TabPane>
      <TabPane tabId='4'>
        <TransferTable from={from} to={to}></TransferTable>
      </TabPane>
    </TabContent>
  </>
}

export default UsersList
