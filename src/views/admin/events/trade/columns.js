import { Badge } from 'reactstrap'


const renderEvent = (type) => {
  if (type === 'new') return (
    <Badge color='success' className='d-block'>
      <span>{type}</span>
    </Badge>
  )
  if (type === 'fill') return (
    <Badge color='info' className='d-block'>
      <span>{type}</span>
    </Badge>
  )
  if (type === 'partial_fill') return (
    <Badge color='warning' className='d-block'>
      <span>{type}</span>
    </Badge>
  )
  return <Badge color='primary' className='d-block'>
    <span>{type}</span>
  </Badge>
}

const renderSide = (side) => {
  if (side === 'buy') return (
    <Badge color='success' className='d-block'>
      <span>Buy</span>
    </Badge>
  )
  if (side === 'sell') return (
    <Badge color='danger' className='d-block'>
      <span>Sell</span>
    </Badge>
  )
}

const renderDate = (date) => {
  return new Date(date).toLocaleDateString("ru-RU")
}

export const columns = [
  {
    name: 'Account ID',
    sortable: true,
    sortField: 'first_name',
    selector: row => row.first_name,
    cell: row => {
      return (
        <div className='d-flex justify-content-left align-items-center'>
          <span className='fw-bolder'>{row.account_id}</span>
        </div>
      )
    }
  },
  {
    name: 'Event',
    sortable: true,
    sortField: 'first_name',
    selector: row => row.first_name,
    cell: row => {
      return (
        <div className='d-flex justify-content-left align-items-center'>
          {renderEvent(row.event)}
        </div>
      )
    }
  },
  {
    name: 'Event ID',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      <div className='d-flex justify-content-left align-items-center'>
        <span className='fw-bolder'>{row.event_id}</span>
      </div>
    </div>
  },
  {
    name: 'Symbol',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      <div className='d-flex justify-content-left align-items-center'>
        <span className='fw-bolder'>{row.order.symbol}</span>
      </div>
    </div>
  },
  {
    name: 'Side',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      <div className='d-flex justify-content-left align-items-center'>
        {renderSide(row.order.side)}
      </div>
    </div>
  },
  {
    name: 'Quantity',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      <div className='d-flex justify-content-left align-items-center'>
        <span className='fw-bolder'>{row.order.qty}</span>
      </div>
    </div>
  },
  {
    name: 'Time in force',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      <div className='d-flex justify-content-left align-items-center'>
        <span className='fw-bolder'>{row.order.time_in_force}</span>
      </div>
    </div>
  },
  {
    name: 'Type',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      <div className='d-flex justify-content-left align-items-center'>
        <span className='fw-bolder'>{row.order.type}</span>
      </div>
    </div>
  },
  {
    name: 'Date',
    cell: row => (
      <div className='column-action'>
        {renderDate(row.at)}
      </div>
    )
  }
]