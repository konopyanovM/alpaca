import { Badge } from 'reactstrap'


const renderDirection = (type) => {
  if (type === 'INCOMING') return (
    <Badge color='success' className='d-block'>
      <span>{type}</span>
    </Badge>
  )
  if (type === 'OUTCOMING') return (
    <Badge color='danger' className='d-block'>
      <span>{type}</span>
    </Badge>
  )
  return <Badge color='primary' className='d-block'>
    <span>{type}</span>
  </Badge>
}

const renderStatus = (type) => {
  if (type === 'APPROVED') return (
    <Badge color='success' className='d-block'>
      <span>{type}</span>
    </Badge>
  )
  if (type === 'ACTIVE') return (
    <Badge color='success' className='d-block'>
      <span>{type}</span>
    </Badge>
  )
  if (type === 'SUBMITTED') return (
    <Badge color='info' className='d-block'>
      <span>{type}</span>
    </Badge>
  )
  if (type === 'INACTIVE') return (
    <Badge color='danger' className='d-block'>
      <span>{type}</span>
    </Badge>
  )
  if (type === '' || !type) return
  // if (type === 'QUEUED') return (
  //   <Badge color='warning' className='d-block'>
  //     <span>{type}</span>
  //   </Badge>
  // )
  return (
    <Badge color='primary' className='d-block'>
      <span>{type}</span>
    </Badge>
  )
}

const renderDate = (date) => {
  return new Date(date).toLocaleDateString("ru-RU")
}

export const columns = [
  {
    name: 'Account ID',
    sortable: true,
    sortField: 'first_name',
    selector: row => row.first_name,
    cell: row => {
      return (
        <div className='d-flex justify-content-left align-items-center'>
          <span className='fw-bolder'>{row.account_id}</span>
        </div>
      )
    }
  },
  {
    name: 'Account number',
    sortable: true,
    sortField: 'first_name',
    selector: row => row.first_name,
    cell: row => {
      return (
        <div className='d-flex justify-content-left align-items-center'>
          <span className='fw-bolder'>{row.account_number}</span>
        </div>
      )
    }
  },
  {
    name: 'Event ID',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      <span className='fw-bolder'>{row.event_id}</span>
    </div>
  },
  {
    name: 'Crypto status from',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {renderStatus(row.crypto_status_from)}
    </div>
  },
  {
    name: 'Crypto status to',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {renderStatus(row.crypto_status_to)}
    </div>
  },
  {
    name: 'Status from',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {renderStatus(row.status_from)}
    </div>
  },
  {
    name: 'Status to',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {renderStatus(row.status_to)}
    </div>
  },
  {
    name: 'Date',
    cell: row => (
      <div className='column-action'>
        {renderDate(row.at)}
      </div>
    )
  }
]