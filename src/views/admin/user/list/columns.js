// ** Reactstrap Imports
import { Badge, Button, Col, Input, Modal, ModalBody, ModalHeader } from 'reactstrap'
import { useState } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { createTransfer } from '../../../../utility/Services'
import toast from 'react-hot-toast'
import { X } from 'react-feather'

const ToastContent = ({ t, name, isError = false, message }) => {
  return (
    <div className="d-flex">
      <div className="d-flex flex-column">
        <div className="d-flex justify-content-between">
          <h6>{name}</h6>
          <X
            size={12}
            className="cursor-pointer"
            onClick={() => toast.dismiss(t.id)}
          />
        </div>
        {
          isError ? <span>
          Ошибка
        </span> : <span>
          {message}
        </span>
        }
       
      </div>
    </div>
  )
}

const renderStatus = (status) => {
  if(status === 'ACTIVE') return (
    <Badge color='success' className='d-block'>
      <span>ACTIVE</span>
    </Badge>
  )
  if(status === 'ACCOUNT_CLOSED') return (
    <Badge color='danger' className='d-block'>
      <span>CLOSED</span>
    </Badge>
  )
  return (
    <Badge color='warning' className='d-block'>
      <span>{status}</span>
    </Badge>
  )
}

const TopUp = ({id, balance, hasAch}) => {
  const [show, setShow] = useState(false)
  const [cardType, setCardType] = useState('')

  const defaultValues = {
    amount: 0
  }

  const {
    reset,
    control,
    setError,
    clearErrors,
    handleSubmit,
    formState: { errors }
  } = useForm({ defaultValues })

  // TODO: toast
  const onSubmit = (data) => {
    console.log(data);
    if (!Object.values(data).every((field) => field.length > 0)) {
      setError('amount', {
        type: 'manual',
        message: 'Please enter a valid amount'
      })
    } else if (data.amount <= 0) {
      setError('amount', {
        type: 'manual',
        message: 'Please enter a valid amount'
      })
    } else {
      const params = {amount: +data.amount, owner: id}
      createTransfer(params).then(()=>{
        toast((t) => (
          <ToastContent
            t={t}
            name={'Transfer'}
            message={'Transfer order created successfully'}
          />
        ))
        setShow(false)
      }).catch((err)=>{
        if (err.response.status === 422) {
          setError('err', {
            type: 'manual',
            message: 'Maximum number of ACH transfers allowed is 1 per trading day in each direction'
          })
        }
      })

    }
    
  }

  return (
    <>
      <Button.Ripple color='primary' disabled={!hasAch} onClick={() => {
        setShow(true)
      }}>Top up</Button.Ripple>
      <Modal
        isOpen={show}
        toggle={() => setShow(!show)}
        className='modal-dialog-centered'
        onClosed={() => setCardType('')}
      >
        <ModalHeader className='bg-transparent' toggle={() => setShow(!show)}></ModalHeader>
        <ModalBody className='px-sm-5 mx-50 pb-5'>
          <h3 className='text-center mb-2'>Top up user balance</h3>
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className='d-flex mb-2 gap-1 align-items-center'>
            <Controller
                  name='amount'
                  control={control}
                  render={({ field }) => {
                    return (
                      <Input
                      type='number'
                      id='amount'
                      value={field.value}
                      onChange={field.onChange}
                      invalid={errors.amount && true}
                      {...field}
                        />
                    )
                  }}
                />
              <h3 className='m-0'>$</h3>
            </div>
            {errors.amount ? <p className='text-danger'>{errors.amount.message}</p> : null}
            {errors.err ? <p className='text-danger'>{errors.err.message}</p> : null}
            <p className='text-center'>Current balance:</p>
            <h4 className='text-center mb-2'>{balance}</h4>
            <Col className='text-center mt-1' xs={12}>
              <Button type='submit' className='me-1' color='primary'>
                Submit
              </Button>
              <Button
                color='secondary'
                outline
                onClick={() => {
                  setShow(!show)
                  reset()
                }}
              >
                Cancel
              </Button>
            </Col>
          </form>

        </ModalBody>
      </Modal></>
  )
}

export const columns = [
  {
    name: 'Name',
    selector: row => row.first_name,
    cell: row => {
      return (
        <div className='d-flex justify-content-left align-items-center'>
          {`${row.first_name} ${row.last_name}`}
        </div>
      )
    }
  },
  {
    name: 'Email',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {row.email}
    </div>
  },
  {
    name: 'Country',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {row.country}
    </div>
  },
  {
    name: 'Balance',
    selector: row => row.role,
    cell: row => <div className='d-flex justify-content-left align-items-center'>
      {row.last_equity}
    </div>
  },
  {
    name: 'Phone number',
    selector: row => row.phone_number,
    cell: row => {
    return <div className='d-flex justify-content-left align-items-center'>
      {row.phone_number}
    </div>
    }
  },
  {
    name: 'Postal code',
    selector: row => row.postal_code,
    cell: row => {
    return <div className='d-flex justify-content-left align-items-center'>
      {row.postal_code}
    </div>
    }
  },
  {
    name: 'Status',
    selector: row => row.postal_code,
    cell: row => {
    return <div className='d-flex justify-content-left align-items-center'>
      {renderStatus(row.status)}
    </div>
    }
  },
  {
    name: 'Actions',
    cell: row => (
      <div className='column-action'>
        <TopUp balance={row.last_equity} id={row.id} hasAch={row.ach_relationship}></TopUp>
      </div>
    )
  }
]