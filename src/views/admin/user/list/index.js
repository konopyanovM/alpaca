// ** User List Component
import Table from './Table'

import Breadcrumbs from '@components/breadcrumbs'

// ** Styles
import '@styles/react/apps/app-users.scss'

const UsersList = () => {
  return (
    <div className='app-user-list'>
      <Breadcrumbs title='Users' data={[{ title: 'Users' }]} />
      <Table />
    </div>
  )
}

export default UsersList
