// ** React Imports
import { Fragment, useState, useEffect } from 'react'

// ** Table Columns
import { columns } from './columns'

// ** Third Party Components
import ReactPaginate from 'react-paginate'
import DataTable from 'react-data-table-component'
import { ChevronDown } from 'react-feather'

// ** Reactstrap Imports
import {
  Card,
  Spinner
} from 'reactstrap'

// ** Styles
import '@styles/react/libs/react-select/_react-select.scss'
import '@styles/react/libs/tables/react-dataTable-component.scss'
import { getAllUsers } from '../../../../utility/Services'

const UsersList = () => {

  // ** States
  const [userList, setUserList] = useState([])
  const [total, setTotal] = useState(0)
  const [currentPage, setCurrentPage] = useState(1)

  const getUsers = async (page = 1) => {
    const { data } = await getAllUsers(`page=${page}`)
    setUserList(data.results)
    setTotal(data.count)
  }

  // ** Function in get data on page change
  const handlePagination = page => {
    getUsers(page.selected + 1)

    setCurrentPage(page.selected + 1)
  }

  // ** Custom Pagination
  const CustomPagination = () => {
    const count = Number(Math.ceil(total / 10))

    return (
      <ReactPaginate
        previousLabel={''}
        nextLabel={''}
        pageCount={count || 1}
        activeClassName='active'
        forcePage={currentPage !== 0 ? currentPage - 1 : 0}
        onPageChange={page => handlePagination(page)}
        pageClassName={'page-item'}
        nextLinkClassName={'page-link'}
        nextClassName={'page-item next'}
        previousClassName={'page-item prev'}
        previousLinkClassName={'page-link'}
        pageLinkClassName={'page-link'}
        containerClassName={'pagination react-paginate justify-content-end my-2 pe-1'}
      />
    )
  }


  useEffect(() => {
    getUsers()
  }, [])

  return (
    <Fragment>
      <Card className='overflow-hidden'>
        {
          userList.length !== 0
            ? <div className='react-dataTable'>
              <DataTable
                noHeader
                sortServer
                pagination
                responsive
                paginationServer
                columns={columns}
                sortIcon={<ChevronDown />}
                className='react-dataTable'
                paginationComponent={CustomPagination}
                data={userList}
              />
            </div>
            : <div className='py-5 d-flex justify-content-center'>
              <Spinner />
            </div>
        }
      </Card>
    </Fragment>
  )
}

export default UsersList
