// ** Dropdowns Imports
import UserDropdown from './UserDropdown'
import NotificationDropdown from './NotificationDropdown'

// ** Third Party Components
import { Sun, Moon } from 'react-feather'

// ** Reactstrap Imports
import { NavItem, NavLink } from 'reactstrap'
import { UserBalance } from './UserBalance'

const NavbarUser = props => {
  // ** Props
  const { skin, setSkin } = props

  // ** Function to toggle Theme (Light/Dark)
  const ThemeToggler = () => {
    if (skin === 'dark') {
      return <Sun className='ficon' onClick={() => setSkin('light')} />
    } else {
      return <Moon className='ficon' onClick={() => setSkin('dark')} />
    }
  }

  return (
    <ul className='nav navbar-nav align-items-center ms-auto w-100'>
      <NavItem className='d-none d-lg-block flex-grow-1 d-flex'>
        <NavLink className='nav-link-style flex-shrink-1' style={{ maxWidth: '30px' }}>
          <ThemeToggler />
        </NavLink>
      </NavItem>
      <NotificationDropdown />
      <UserBalance />
      <UserDropdown />
    </ul>
  )
}
export default NavbarUser
