import { useEffect, useState } from "react"

// ** Utils
import { getBalance } from "../../../../utility/Services"

export const UserBalance = () => {

  // ** State
  const [balance, setBalance] = useState(0)

  useEffect(() => {
    getBalance().then((res) => {
      setBalance(res.data.balance)
    })
  }, [])

  return (
    <div style={{ margin: '0 10px', display: 'flex', flexDirection: 'column' }}>
      <span>Balance</span>
      <h6 className="fw-bold">${balance}</h6>
    </div>
  )
}