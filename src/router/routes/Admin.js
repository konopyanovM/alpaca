import { lazy } from 'react'

const Users = lazy(() => import('../../views/admin/user/list/index'))
const Events = lazy(() => import('../../views/admin/events'))

const AdminRoutes = [
  {
    element: <Users />,
    path: '/admin/users'
  },
  {
    element: <Events />,
    path: '/admin/events'
  }
]

export default AdminRoutes
