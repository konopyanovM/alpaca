import { lazy } from 'react'

const AccountSettings = lazy(() => import('../../views/pages/account-settings'))
const Watchlist = lazy(() => import('../../views/pages/watchlist'))
const TransactionHistory = lazy(() => import('../../views/pages/transaction-history'))
const OrdersAndPositions = lazy(() => import('../../views/pages/order'))
const Documents = lazy(() => import('../../views/pages/documents'))
const StockPage = lazy(() => import('../../views/pages/stockPage'))
const Disclosures = lazy(() => import('../../views/pages/disclosures'))


const PagesRoutes = [
  {
    path: '/account-settings',
    element: <AccountSettings />
  },
  {
    path: '/watchlist',
    element: <Watchlist />
  },
  {
    path: '/orders-positions',
    element: <OrdersAndPositions />
  },
  {
    path: '/transaction-history',
    element: <TransactionHistory />
  },
  {
    path: '/documents',
    element: <Documents />
  },
  {
    path: '/stocks/',
    element: <StockPage />
  },
  {
    path: '/stocks/:symbol',
    element: <StockPage />
  },
  {
    path: '/disclosures',
    element: <Disclosures />
  },
]

export default PagesRoutes
