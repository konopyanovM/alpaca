import axios from 'axios'

const BASE_URL = 'https://app-trade1.investlink.io/'

export const getBalance = () => axios.get(`${BASE_URL}alpaca/check_balance/`)

export const getAchRelationship = () => axios.get(`${BASE_URL}alpaca/get_ach_relationship/`)

export const createACHRel = (params) => axios.post(`${BASE_URL}alpaca/create_ach_relationship/`, params)

export const getAllUsers = (query = '') => axios.get(`${BASE_URL}alpaca/list_accounts/${query && `?${query}`}`)

export const getTransactionHistory = () => axios.get(`${BASE_URL}alpaca/transaction_history/`)

export const getPositions = () => axios.get(`${BASE_URL}alpaca/get_all_positions/`)

export const createTransfer = (params) => axios.post(`${BASE_URL}alpaca/create_transfer/`, params)

export const createOrder = (params) => axios.post(`${BASE_URL}alpaca/create_order/`, params)

export const getStockBars = (params) => axios.post(`${BASE_URL}alpaca/stock_bars/`, params)

export const getLatestStockBar = (params) => axios.post(`${BASE_URL}alpaca/latest_stock_bar/`, params)

export const getDocuments = () => axios.get(`${BASE_URL}alpaca/account_documents/`)

export const downloadDocument = (document_id) => axios({
    url: `${BASE_URL}alpaca/download_document/`,
    method: 'POST',
    responseType: 'blob',
    data: { document_id },
}).then((response) => {
    const href = URL.createObjectURL(response.data);

    const link = document.createElement('a');
    link.href = href;
    link.setAttribute('download', `${document_id}.pdf`);
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
    URL.revokeObjectURL(href);
});

export const getDocumentInfo = (document_id) => axios.post(`${BASE_URL}alpaca/download_document/`, { document_id })

export const getUserOrders = () => axios.get(`${BASE_URL}alpaca/user_orders/`)

export const getEvent = (since, until, type = 'account') => {
    switch (type) {
        case 'account':
            return axios.get(`${BASE_URL}alpaca/event_account_status?since=${since}&until=${until}`)
        case 'journal':
            return axios.get(`${BASE_URL}alpaca/event_journal_status?since=${since}&until=${until}`)
        case 'trade':
            return axios.get(`${BASE_URL}alpaca/event_trade_update?since=${since}&until=${until}`)
        case 'transfer':
            return axios.get(`${BASE_URL}alpaca/event_transfer?since=${since}&until=${until}`)
    }
}

export const cancelOrder = (params) => axios.post(`${BASE_URL}alpaca/cancel_order/`, params)

export const uploadDocument = (params) => axios.post(`${BASE_URL}alpaca/upload_document/`, params)

// Auth

export const deleteAccount = () => {
    return axios.delete(`${BASE_URL}auth/delete_user/`)
}

export const updateDetail = (id, params) => {
    return axios.patch(`${BASE_URL}auth/update_user/${id}/`, params)
}

export const getUserDetail = () => axios.get(`${BASE_URL}auth/user_detail/`)
